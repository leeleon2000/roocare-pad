//
//  ServiceCharacteristic.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/22.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import RxBluetoothKit
import CoreBluetooth

enum DeviceService: String, ServiceIdentifier {
    case SERVICE = "FFF0"

    var uuid: CBUUID {
        return CBUUID(string: self.rawValue)
    }
}


enum DeviceCharacteristic: String, CharacteristicIdentifier {
    case WRITE = "FFF2"
    case READ = "FFF1"

    
    var uuid: CBUUID {
        return CBUUID(string: self.rawValue)
    }
    
    //Service to which characteristic belongs
    var service: ServiceIdentifier {
        switch self {
        case .WRITE, .READ:
            return DeviceService.SERVICE
        }
    }
}
