//
//  Device.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/7/26.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import RxBluetoothKit
import SwiftDate
import SwifterSwift
import CoreBluetooth

let deviceManager = DeviceManager()

struct DeviceScanData {
    let scanDevice: ScannedPeripheral
    let decodedData: ScanResult_1001_def?
    var createTime = Date()
}

struct ConnectScanData {
    let scanDevice: ScannedPeripheral
    let mac: String
}


class DeviceManager{
    
    private let centerManager = CentralManager()
    private var disposeBag = DisposeBag()
    private var scanBag = DisposeBag()
    
    public let scanDataList = BehaviorRelay<[String: DeviceScanData]>(value: [:])
    public let scanConnectList = BehaviorRelay<[String: ConnectScanData]>(value: [:])
    public let error = PublishRelay<String>()
    
    var uuidDeviceMap: [String: DeviceRecord] = [:]

    
    public init() {
        
        PadDataBase.instance.deviceList.subscribe(onNext: { (list: [DeviceRecord]) in
                                                    self.uuidDeviceMap += Dictionary(uniqueKeysWithValues: list.filter{!$0.mac.isEmpty}.map{ (device: DeviceRecord) in
                (device.uuid, device)
            })}
        )
        PadDataBase.instance.loadDeviceList()
    }
    
    private func autoRemoveDisappearedDevice(){
        Observable<Int>.interval(RxTimeInterval.seconds(10), scheduler: MainScheduler.instance).subscribe(onNext: { _ in
            print(self.scanDataList.value)
            self.scanDataList.accept(self.scanDataList.value.filter{ (Date().second - $0.value.createTime.second) < 20
            })
        }).disposed(by: disposeBag)
    }
    
    public func bleState() -> Observable<BluetoothState> {
        return centerManager
            .observeState()
            .startWith(centerManager.state)
    }
    
    private func powerOnState() -> Observable<BluetoothState>{
        return bleState().filter { $0 == .poweredOn }.take(1)
    }
    
    public func startScanData(){
        print("startScanData")
        autoRemoveDisappearedDevice()
        powerOnState().flatMap({ _ in
            self.centerManager.scanForPeripherals(withServices: nil)        
        }).subscribe(onNext: { device in
            self.tryDecodeData(peripheral: device)
        }, onError: {
            self.error.accept($0.localizedDescription)
        }).disposed(by: scanBag)
    }
    
    public func tryDecodeData(peripheral: ScannedPeripheral) {
     //   print("data:\(peripheral.advertisementData.advertisementData)")

        if let data: Data = peripheral.advertisementData.manufacturerData{
            var scanResult = decodeScan(data.bytes, Int32(data.count), 0)
            var mac: Int64 = 0
            if(scanResult.success == 2){
                let dataBuffer = [UInt8](UnsafeBufferPointer(start: &scanResult.MAC_addr.0, count: 6))
                let d = Data(bytes: dataBuffer)
                mac = d.int64()
            }
            let uuid = peripheral.peripheral.identifier.uuidString
            print("raw data: \(data.hexEncodedString())")

            if(mac != 0 && settingManager.isLicensed){
                print("store mac:\(mac)")
                if(uuidDeviceMap[uuid] == nil){
                    uuidDeviceMap[uuid] = DeviceRecord(id: -1, mac: String(mac))
                }
                self.addOrUpdateScanList(peripheral: peripheral, mac: String(mac))
//                let auth = shtp_auth_def(success: 0, key: (0, 0, 0))
//                var cmd = gen_cmd_req_access(auth)
//                let dataBuffer = [UInt8](UnsafeBufferPointer(start: &cmd.data.0, count: MemoryLayout.size(ofValue: cmd.data_len)))
//                let data = Data(dataBuffer)
//                peripheral.peripheral.establishConnection().flatMap {_ in
//                    peripheral.peripheral.writeValue(data, for: DeviceCharacteristic.WRITE, type: .withoutResponse)
//                }
//                .delay(.seconds(2), scheduler: MainScheduler.instance)
//                .flatMap{ _ -> (Single<Characteristic>) in
//                    peripheral.peripheral.readValue(for: DeviceCharacteristic.READ)
//                }.subscribe(onNext: { ch in
//                    var bytes = ch.value!.bytes
//                    var def = shtp_pkg_def()
//                    print(bytes)
//                    //unsafeCopyToTuple(array: &bytes, to: &def.data)
//                  //  chk_ack_req_access(
//                })
                
            }else if(uuidDeviceMap[uuid] != nil){
                let device = uuidDeviceMap[uuid]!
                print("mac:\(device.mac) data: \(data.hexEncodedString())")
                let scanData = decodeScan(data.bytes, Int32(data.count), Int64(device.mac)!)
                if(scanResult.success != 1){
                    return
                }
                var old = self.scanDataList.value
                old[uuid] = DeviceScanData(scanDevice: peripheral, decodedData: scanData)
                self.scanDataList.accept(old)
                                
                if(settingManager.shouldShowHighAlert(temperature: scanData.skinTemp)){
                    notificationManager.save(record: NotificationRecord(title: "\(device.name) High Temp Alert" , date: Date(), temp: Double(scanData.skinTemp), deviceID: device.id))
                }
                
                PadDataBase.instance.saveTemperature(temperature: BodyTemperatureRecord(deviceID: device.id!, value: Double(scanData.skinTemp)))
                print("scan data \(scanData)")
            }
        }
    }
    
    private func addOrUpdateScanList(peripheral: ScannedPeripheral, mac: String){
        let uuid = peripheral.peripheral.identifier.uuidString
        var new = self.scanConnectList.value
        new[uuid] = ConnectScanData(scanDevice: peripheral, mac: mac)
        self.scanConnectList.accept(new)
    }
    
    public func stopScan(){
        scanBag = DisposeBag()
    }
    
    public func stop(){
        disposeBag = DisposeBag()
        stopScan()
    }
}




/// Unsafe copy array to tuple
func unsafeCopyToTuple<ElementType, Tuple>(array: inout Array<ElementType>, to tuple: inout Tuple) {
    withUnsafeMutablePointer(to: &tuple) { pointer in
        let bound = pointer.withMemoryRebound(to: ElementType.self, capacity: array.count) { $0 }
        array.enumerated().forEach { (bound + $0.offset).pointee = $0.element }
    }
}
