//
//  PadDataBase.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/6.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import GRDB
import RxSwift
import RxCocoa
// device table: id / model / mac
// user table: name / deviceList / phone 


let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)

class PadDataBase{
    // Simple database connection
    var dbQueue: DatabaseQueue = try! DatabaseQueue(path: getDirPath())
    let sqlDateFormat = "yyyy-MM-dd HH:00"
    let deviceList = BehaviorRelay<[DeviceRecord]>(value: [])
    
    init() {
        var migrator = DatabaseMigrator()
        migrator.registerMigration("1.0") { (db) in
            BodyTemperatureRecord.addMigration(db: db)
            DeviceRecord.addMigration(db: db)
            NotificationRecord.addMigration(db: db)
        }
        try! migrator.migrate(dbQueue)
    }
    
    func findDeviceRecord(deviceID: Int64)-> DeviceRecord?{
        return deviceList.value.first(where: {
            $0.id == deviceID
        })
    }
    
    func saveTemperature(temperature: BodyTemperatureRecord){
        if(temperature.value == 30.0){
            return
        }
        
        var record = temperature
        do{
            try dbQueue.write{
                try record.insert($0)
                print("save temp success: \(record)")
            }
        }catch{
            print("save error:\(error)")
        }
    }
    
    func loadTemperature(start: Date, end: Date, dateFormat: String, deviceID: Int64) -> [BodyTemperatureRecord]{
        dbQueue.read{ db in
            let res = try! Row.fetchAll(db, sql:
                                            "select strftime('\(dateFormat)', date) as s, * from bodyTemperatureRecord where deviceID = '\(deviceID)' group by strftime('\(dateFormat)', date)")
                .map{
                    BodyTemperatureRecord(id: $0["id"], deviceID: $0["deviceID"], value: $0["value"], date: $0["date"])
                }
            return res
        }
    }
    
    func saveDevice(device: DeviceRecord) -> DeviceRecord{
        var record = device
        if(record.uuid.isEmpty){
            record.uuid = UUID().uuidString
        }
        do{
            try dbQueue.write{
                try record.insert($0)
                print("save device success: \(record)")
            }
        }catch{
            print("save error:\(error)")
        }
        
        let list = dbQueue.read{ db in
            return try! DeviceRecord.fetchAll(db)
        }
                
        loadDeviceList()
        return list.last!

    }
    
    func deleteDevice(device: DeviceRecord){
        var record = device
        do{
            try dbQueue.write{
                try record.delete($0)
                print("delete device success: \(record)")
            }
        }catch{
            print("delete error:\(error)")
        }
        loadDeviceList()
    }
            
    func saveNotification(notification: NotificationRecord){
        var record = notification
        do{
            try dbQueue.write{
                try record.insert($0)
                print("saveNotification success: \(record)")
            }
        }catch{
            print("saveNotification error:\(error)")
        }
    }
    
    func loadNotificationList() -> [NotificationRecord]{
        let list = dbQueue.read{ db in
            return try! NotificationRecord
                .order(Column("date").desc)
                .group(Column("deviceID"))
                .fetchAll(db)
//                .fetchAll(db, sql: "select id, title, date, deviceID from \(NotificationRecord.databaseTableName) order by 'id' desc, group by 'deviceID'")
        }
        return list
    }
    
    func loadDeviceList(){
        self.deviceList.accept(
            dbQueue.read{ db in
                return try! DeviceRecord.fetchAll(db)
            })
    }
    
    static func getDirPath() -> String{
        let databaseURL = try! FileManager.default
            .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            .appendingPathComponent("db.sqlite")
        return databaseURL.path
    }
    
    static let instance = PadDataBase()
}

class UserRecord: Record{
    var id: Int64? = nil
    var name: String = ""
    var phone: String = ""
    
    /// The table name
    override class var databaseTableName: String { "user" }
    
    /// The table columns
    enum Columns: String, ColumnExpression {
        case id, name, phone
    }
}

struct DeviceRecord: Hashable{
    var id: Int64? = nil
    var uuid: String = UUID().uuidString
    //var userid: Int64 = 0
    var phone: String = ""
    var name: String = ""
    // var modelName: String = ""
    var mac: String = ""
    
    static func addMigration(db: Database) {
        print("create table: \(DeviceRecord.databaseTableName) ")
        try! db.create(table: String(DeviceRecord.databaseTableName), body: { (t) in
            t.autoIncrementedPrimaryKey("id", onConflict: .replace)
            t.column("uuid", .text).unique(onConflict: .replace)
            t.column("phone", .text)
            t.column("name", .text)
            t.column("mac", .text)
        })
    }
    
    static var databaseTableName: String = "DeviceRecord"
}


extension DeviceRecord: Codable, FetchableRecord, MutablePersistableRecord {
    // Define database columns from CodingKeys
    fileprivate enum Columns {
        static let id = Column(CodingKeys.id)
        static let uuid = Column(CodingKeys.uuid)
        static let phone = Column(CodingKeys.phone)
        static let name = Column(CodingKeys.name)
        static let mac = Column(CodingKeys.mac)
    }
    
    func get(db: Database) {
        try! db.execute(sql: "Select * from DeviceRecord")
    }
    
    /// Updates a player id after it has been inserted in the database.
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}
