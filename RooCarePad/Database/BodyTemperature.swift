//
//  BodyTemperature.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/8.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import GRDB

struct BodyTemperatureRecord: Hashable{
    var id: Int64? = nil
    var deviceID: Int64 = 0
    var value: Double = 0.0
    var date: Date = Date()

    static func addMigration(db: Database) {
        print("create table: \(BodyTemperatureRecord.databaseTableName) ")
        try! db.create(table: String(BodyTemperatureRecord.databaseTableName), body: { (t) in
            t.autoIncrementedPrimaryKey("id")
            t.column("deviceID", .integer)
            t.column("value", .double)
            t.column("date", .integer)
        })
    }
    
    static func getTestData() -> BodyTemperatureRecord{
        return BodyTemperatureRecord(value: Double.random(in: 30...40), date: Date())
    }
}


extension BodyTemperatureRecord: Codable, FetchableRecord, MutablePersistableRecord {
    // Define database columns from CodingKeys
    fileprivate enum Columns {
        static let id = Column(CodingKeys.id)
        static let deviceID = Column(CodingKeys.deviceID)
        static let value = Column(CodingKeys.value)
        static let date = Column(CodingKeys.date)
    }

    func get(db: Database) {
        try! db.execute(sql: "Select avg(value) from BodyTemperatureRecord")
    }
    
    /// Updates a player id after it has been inserted in the database.
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}
