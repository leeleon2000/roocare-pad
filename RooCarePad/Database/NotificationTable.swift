//
//  NotificationTable.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/14.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import GRDB

struct NotificationRecord: Hashable{
    var id: Int64? = nil
    var title: String = ""
    var date: Date = Date()
    var temp: Double = 0
    var deviceID: Int64? = nil

    static func addMigration(db: Database) {
        print("create table: \(NotificationRecord.databaseTableName) ")
        try! db.create(table: String(NotificationRecord.databaseTableName), body: { (t) in
            t.autoIncrementedPrimaryKey("id")
            t.column("deviceID", .integer)
            t.column("title", .text)
            t.column("date", .integer)
            t.column("temp", .text)

        })
    }    
//    static func getTestData() -> BodyTemperatureRecord{
//        return BodyTemperatureRecord(value: Double.random(in: 30...40), date: Date())
//    }
}


extension NotificationRecord: Codable, FetchableRecord, MutablePersistableRecord {
    // Define database columns from CodingKeys
    fileprivate enum Columns {
        static let id = Column(CodingKeys.id)
        static let title = Column(CodingKeys.title)
        static let date = Column(CodingKeys.date)
        static let deviceID = Column(CodingKeys.deviceID)
        static let temp = Column(CodingKeys.temp)
      //  static let tempertaure = Column(CodingKeys.temperature)
    }
    /// Updates a player id after it has been inserted in the database.
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}
