//
//  UIColor+Name.swift
//  foot pad
//
//  Created by Yu-An Hsiao on 2020/2/9.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import UIKit
import Foundation
import SwiftUI


extension UIColor {
    
    public static let MainColor = UIColor(named: "MainColor") ?? UIColor.black
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
    
    public var color: Color{
        get{
            Color(self)
        }
    }
}
