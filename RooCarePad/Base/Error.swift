//
//  Error.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/10.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

extension String: LocalizedError {
    public var errorDescription: String? { return self }
}
