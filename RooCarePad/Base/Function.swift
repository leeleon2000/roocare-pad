//
//  Function.swift
//  AuthMeSDK
//
//  Created by Leon Lee on 2020/9/9.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import UIKit

protocol HasApply { }

extension HasApply {
    func apply(closure:(Self) -> ()) -> Self {
        closure(self)
        return self
    }
    
    func with<R>(block: (Self) -> R) -> R {
        return block(self)
    }
}

extension NSObject: HasApply { }
extension Date: HasApply{}
extension BaseViewModel: HasApply{}
