//
//  BaseViewModel.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/6.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Combine


protocol ErrorModel{
    var title : String { get }
    var description : String { get }
}

struct BasicErrorModel : ErrorModel{
    let title : String
    let description : String
}

public class BaseViewModel{
    @Published var viewModelKey = 0
    var disposeBag = DisposeBag()
    let error = PublishRelay<ErrorModel>()
    var isStart = false
    
    open func onError(_ title : String, _ description : String){
        error.accept(BasicErrorModel(title: title, description: description))
    }
    
    open func start(){
        if(!isStart){
            isStart = true
            onStart()
        }
    }
    
    internal func onStart(){
        
    }
    
    open func stop(){
        disposeBag = DisposeBag()
        isStart = false
    }
}
