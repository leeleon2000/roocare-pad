//
//  RadioButtonField.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import SwiftUI

//MARK:- Single Radio Button Field
struct RadioButtonField: View {
    let id: String
    let label: String
    let size: CGFloat
    let color: Color
    let textSize: CGFloat
    let isMarked:Bool
    let callback: (String)->()
    
    init(
        id: String,
        label:String,
        size: CGFloat = 24,
        color: Color = Color.black,
        textSize: CGFloat = 16,
        isMarked: Bool = false,
        callback: @escaping (String)->()
        ) {
        self.id = id
        self.label = label
        self.size = size
        self.color = color
        self.textSize = textSize
        self.isMarked = isMarked
        self.callback = callback
    }
    
    var body: some View {
        Button(action:{
            self.callback(self.id)
        }) {
            HStack(alignment: .center, spacing: 10) {
                Image(systemName: self.isMarked ? "largecircle.fill.circle" : "circle")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: self.size, height: self.size)
                Text(label)
                    .font(Font.system(size: textSize))
                Spacer()
            }.foregroundColor(UIColor.MainColor.color)
        }
        .foregroundColor(Color.white)
    }
}


struct RadioButtonGroups: View {
    let names: [String]
    let callback: (String) -> ()
    
    @State var selectedId: String = ""
    
    var body: some View {
        VStack {
            ForEach(names){ name in
                RadioButtonField(
                    id: name,
                    label: name,
                    isMarked: selectedId == name ? true : false,
                    callback: radioGroupCallback
                ).padding(8)
            }
        }
    }

    func radioGroupCallback(id: String) {
        selectedId = id
        callback(id)
    }
}

extension String: Identifiable {
    public var id: String { self }
}
