//
//  View+.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/7.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import SwiftUI

extension View{
    @inlinable public func defaultOutline() -> some View{
        return self.frame(minWidth: 0,maxWidth: .infinity)
            .padding(12)
            .overlay(RoundedRectangle(cornerRadius: 20).stroke(UIColor.MainColor.color, style: StrokeStyle(lineWidth: 3)))
    }
 
    @inlinable public func defaultSolidOutline() -> some View{
        return self.padding(12)
            .background(UIColor.MainColor.color)
            .foregroundColor(Color.white)
            .cornerRadius(20)
    }
}

