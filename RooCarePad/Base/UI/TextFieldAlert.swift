//
//  TextFieldAlert.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/19.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import SwiftUI

struct TextFieldAlert<Presenting>: View where Presenting: View {
    
    @Binding var isShowing: Bool
    let save: () -> ()
    @Binding var text: String
    let presenting: Presenting
    let title: String
    
    var body: some View {
        GeometryReader { (deviceSize: GeometryProxy) in
            ZStack(alignment: .top) {
                self.presenting
                    .disabled(self.isShowing)                
                VStack {
                    Text(self.title)
                    TextField("Name", text: self.$text)                
                        .textFieldStyle(RoundedBorderTextFieldStyle())
//                        .padding(4)
//                        .overlay(
//                            RoundedRectangle(cornerRadius: 5)
//                                .stroke(Color.gray.opacity(0.3), lineWidth: 1)
//                        )
         //           Divider()
                    HStack() {
                        Spacer()
                        Button(action: {
                            self.isShowing.toggle()
                        }) {
                            Text("Dismiss")
                        }
                        Spacer()
                        Button(action: {
                            self.save()
                            self.isShowing.toggle()
                        }) {
                            Text("Add")
                        }
                        Spacer()
                    }
                }
                .padding()
                .background(Color.white)
                .overlay(RoundedRectangle(cornerRadius: 20).stroke(UIColor.MainColor.color, style: StrokeStyle(lineWidth: 3)).shadow(radius: 1))
                .frame(
                    width: deviceSize.size.width*0.7,
                    height: deviceSize.size.height*0.7
                )
                // .shadow(radius: 1)
                .opacity(self.isShowing ? 1 : 0)
            }
        }
    }
}

extension View {
    
    func textFieldAlert(isShowing: Binding<Bool>,
                        save: @escaping () -> (),
                        text: Binding<String>,
                        title: String) -> some View {
        TextFieldAlert(isShowing: isShowing,
                       save: save,
                       text: text,
                       presenting: self,
                       title: title)
    }
    
}
