//
//  BLE.swift
//  foot pad
//
//  Created by Leon Lee on 2020/2/2.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation


func getPointer(obj: AnyObject) -> UnsafeMutableRawPointer{
    return Unmanaged.passUnretained(obj).toOpaque()
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
    
    func BiOperation(other : Data, op : (UInt8, UInt8) -> UInt8) -> Data?{
        if(other.count != count) {
            return nil
        }
        
        var data = Data.init(count: self.count)
        for i in 0 ..< self.count {
            data[i] = op(self[i], other[i])
        }
        
        return data
    }
    
    public func and(other : Data) -> Data?{
        return BiOperation(other: other, op: { $0 & $1 })
    }
    
    public func xor(other: Data) -> Data?{
        return BiOperation(other: other, op: { $0 ^ $1 })
    }
    
    public func or(other: Data) -> Data?{
        return BiOperation(other: other, op: { $0 | $1 })
    }
    
    public func add(other: Data) -> Data?{
        return BiOperation(other: other, op: { UInt8((Int($0) + Int($1)) % 256)})
    }
    
    var bytes : [UInt8]{
        return [UInt8](self)
    }
    
    func fillBytes(bytes : Int) -> Data{
        if(self.count == bytes){
            return self
        }
        
        var array : [UInt8] = Array(repeating: 0, count: bytes)
        for i in (0..<self.count) {
            array[bytes - count + i] = self[i]
        }
        
        return Data(bytes: &array, count: bytes)
    }
    
    func swapUInt16Data() -> Data {
        var mdata = self // make a mutable copy
        let count = self.count / MemoryLayout<UInt16>.size
        mdata.withUnsafeMutableBytes { (i16ptr: UnsafeMutablePointer<UInt16>) in
            for i in 0..<count {
                i16ptr[i] =  i16ptr[i].byteSwapped
            }
        }
        return mdata
    }
    
    func to_<T>() -> T{
        return withUnsafeBytes {
            (pointer: UnsafePointer<T>) -> T in
            return pointer.pointee // reading four bytes of data
        }
    }
    
    func int64() -> Int64{
        return to_()
    }
    
    func uint32() -> UInt32{
        return to_()
    }
    
    func uint16() -> UInt16{
        return to_()
    }
    
    func uint8() -> UInt8{
        return to_()
    }
    
    func sint8() -> Int8{
        return to_()
    }
    
    func asString() -> String{
        return String(data: self, encoding: String.Encoding.ascii) ?? ""
    }
    
    static func from_<T>(value : T) -> Data {
        var ref = value
        return Data(bytes: &ref,
             count: MemoryLayout.size(ofValue: ref))
    }
    
    static func from(value : UInt16) -> Data{
        return from_(value: value)
    }
    
    static func from(value : UInt8) -> Data{
        return from_(value: value)
    }
 
    static func from(value : UInt32) -> Data{
        return from_(value: value)
    }

    static func from(value : String) -> Data{
        var v = value.cString(using: String.Encoding.ascii)!
        return Data.init(bytes: &v, count: v.count)
    }
}

extension String {
    var hexadecimal: Data? {
        var data = Data(capacity: count / 2)
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }
        
        guard data.count > 0 else { return nil }
        return data
    }
    
    func toUInt() -> UInt? {
        let scanner = Scanner(string: self)
        var u: UInt64 = 0
        if scanner.scanUnsignedLongLong(&u)  && scanner.isAtEnd {
            return UInt(u)
        }
        return nil
    }
}
