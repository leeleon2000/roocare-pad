
// 01e1110000220033440000000000005566

#include "mbsn_adv_decode.h"
#include <iomanip>
#include <ios>
#include <ctime>
#include <iostream>
#include <time.h>
#include <stdio.h>
using namespace std;

uint64_t dummy_UNIXms = 0;

struct tm newtime;
//__time32_t aclock;

uint64_t timeStamp_parse(ScanResult_1001_def* res, const uint8_t* timeStamp)
{

    //    _time32(&aclock);                  // Get time in seconds.
    //    _localtime32_s(&newtime, &aclock); // Convert time to struct tm form.

    // Print local time as a string.
#if 0
    char    buffer[32];
    errno_t errNum;
    errNum = asctime_s(buffer, 32, &newtime);
    if (errNum) {
        printf("Error code: %d", (int)errNum);
        return 1;
    }
    printf("Current date and time: %s", buffer);
#endif

    res->time_UTC = 0;
    // get phone time
    time_t time_buf;
    time(&time_buf);
    res->time_UTC = time_buf;
    printf("time_UTC: %d \r\n", (int)res->time_UTC);

    if (timeStamp[0] & 0x80) { // high resolution
        res->time_ms = ((timeStamp[1] & 0x07) << 8) | timeStamp[2];
    } else { // low resolution
        if (timeStamp[2] & 0x01) {
            res->time_ms = 500;
        } else {
            res->time_ms = 0;
        }
    }

    dummy_UNIXms += 1000;
    return dummy_UNIXms;
}

ScanResult_1001_def mbsn_1001_decode(const uint8_t* data, int length, uint64_t mac)
{
    ScanResult_1001_def ScanResult_1001;

    uint8_t timeStampBuf[3];

#if 0
    cout << "Encoded data=";
    for (uint8_t i = 0; i < 17; i++){
        cout << static_cast<int> (data[i]) << ",";
    }
    cout << "\r\n";
#endif

    uint8_t* newData = new uint8_t[length];
    memcpy(newData, data, length);

    // catch decode type
    if (newData[9] & 0x80) {
        ScanResult_1001.success = 2;
        // mac addr in package

        // catch encoded data from package
        uint8_t encode_buf[6];
        memcpy(&encode_buf[0], &newData[TIME_STAMP_INDEX], 4); // copy timestamp & report
        memcpy(&encode_buf[4], &newData[TEMP_INDEX_1001], 2);  // copy temperature
        uint8_t key = newData[DECODE_KEY_INDEX_1001];

        // get MAC
        ScanResult_1001.MAC_addr[0] = newData[7];
        ScanResult_1001.MAC_addr[1] = newData[8];
        ScanResult_1001.MAC_addr[2] = newData[10];
        ScanResult_1001.MAC_addr[3] = newData[11];
        ScanResult_1001.MAC_addr[4] = newData[12];
        ScanResult_1001.MAC_addr[5] = newData[13];
        uint64_t mac_buf            = newData[7] << 40 | newData[8] << 32 | newData[10] << 24 | newData[11] << 16
            | newData[12] << 8 | newData[13];

        // decode
        App_cipherEnDeCode(&encode_buf[0], 6, key, mac_buf);

        // replace data
        memcpy(&newData[TIME_STAMP_INDEX], &encode_buf[0], 4); // replace timestamp & report
        memcpy(&newData[TEMP_INDEX_1001], &encode_buf[4], 2);  // replace temperature

    } else {
        ScanResult_1001.success = 1;
        // phone num. in package

        // decode
        App_cipherEnDeCode(&newData[DECODE_START_INDEX_1001], DECODE_LEN_1001, newData[DECODE_KEY_INDEX_1001], mac);

        // get phone national code
        memcpy(&ScanResult_1001.nationalCode, &newData[NATIONAL_CODE_INDEX_1001], NATIONAL_CODE_LEN_1001);
        cout << "1001 National code = " << static_cast<int>(ScanResult_1001.nationalCode) << endl;

        // get phone
        // parse to char
        // char temp[] = { '0', '9', '1', '2', '1', '2', '3', '4', '5', '6' };
        // memcpy(ScanResult_1001.phoneCode, temp, sizeof(temp));
        memcpy(&ScanResult_1001.phoneCode, &newData[PHONE_CODE_INDEX_1001], PHONE_CODE_LEN_1001);
        cout << "1001 Phone code = " << hex << ScanResult_1001.phoneCode << endl;
    }

    // parse time stamp
    memcpy(timeStampBuf, &newData[TIME_STAMP_INDEX], TIME_STAMP_LEN);
    timeStamp_parse(&ScanResult_1001, timeStampBuf);
    // cout << "device time  = " <<ctime_s() << endl;
    cout << "device time ms = " << static_cast<int>(ScanResult_1001.time_ms) << endl;

    // get system report
    ScanResult_1001.report = newData[SYS_RPT_INDEX];
    cout << "system report = " << static_cast<int>(ScanResult_1001.report) << endl;

    // get temp data
    int16_t temp;
    memcpy(&temp, &newData[TEMP_INDEX_1001], sizeof(temp));

    // parse alarm
    if (temp & 0xC000) {
        ScanResult_1001.alarm = 1;
    }
    cout << "1001 Temp alarm = " << static_cast<int>((uint8_t)(newData[TEMP_INDEX_1001 + 1] & 0xC0) >> 7) << endl;

    // ScanResult_1001.alarm = newData[1001_TEMP_INDEX + 1] & 0x80;
    // cout << "1001 Temp alarm = " << static_cast<int>((uint8_t)(newData[1001_TEMP_INDEX + 1] & 0x80) >> 7) << endl;
    // transfer temp data to float

    // parse temp
    if (temp & 0x0800) {                                                   // neg
        ScanResult_1001.skinTemp = (float)((int16_t)(temp | 0xF000) / 10.0); // set sign
    } else {                                                               // pos
        ScanResult_1001.skinTemp = (float)((int16_t)(temp & 0x0FFF) / 10.0); // clear sign
    }

    // int16_t temp;
    // ScanResult_1001.skinTemp = (float)((uint8_t)(newData[1001_TEMP_INDEX] & 0x7F) / 10.0) + 30;
    // cout << "1001 Skin Temp = " << dec << (ScanResult_1001.skinTemp) << endl;

//    ScanResult_1001.success = 1;
    // send call back back
    return ScanResult_1001;
}

int64_t decodeMac(const uint8_t* data, const int length)
{
    int64_t mac = 0;
    if ((data[0] == 0x27) || (data[0] == 0x28)) {
        memcpy(&mac, &data[7], 6);
    }
    return mac;
}

ScanResult_1001_def decodeScan(const uint8_t* data, const int length, int64_t mac)
{

    uint16_t devId;

    // get device id
    memcpy(&devId, &data[DEV_ID_INDEX], DEV_ID_LEN);

    std::cout << "devId=" << std::hex << devId << "\r\n";

    switch (devId) {
    case 0x1001:
        return mbsn_1001_decode(data, length, mac);
        break;
        // add other device call back
    default:
        break;
    }
    ScanResult_1001_def failed;
    failed.success = 0;
    return failed;
}
