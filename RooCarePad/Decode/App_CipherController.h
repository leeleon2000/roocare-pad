
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#ifndef MBSN_CIPHERCONTROLLER_H_
#define MBSN_CIPHERCONTROLLER_H_

extern uint8_t App_cipherWord_record;
extern uint8_t App_cipherEnDeCode(uint8_t* ptr, uint8_t ptr_len, uint8_t cipherWord, uint64_t mac);

#endif
