
#include "mbsn_conn_lib.h"
#include <iostream>



// write config of the seq_id
shtp_pkg_def gen_cmd_wr_cfg(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD] = SHTP_CMD__WR;
    pkg.data[SHTP_IDX_ID]  = buf.seq_id;

    uint16_t len = buf.data_len + 2; // 2 is check sum
    memcpy(&pkg.data[SHTP_IDX_LEN_LSB], &len, sizeof(len));

    memcpy(&pkg.data[SHTP_IDX_DATA], buf.data, buf.data_len);

    // check sum
    uint16_t chksum = 0;
    for (uint16_t i = 0; i < buf.data_len; i++) {
        chksum += buf.data[i];
    }
    memcpy(&pkg.data[SHTP_IDX_DATA + buf.data_len], &chksum, sizeof(chksum));

    pkg.data_len = 4 + buf.data_len + sizeof(chksum);
    pkg.success  = 1;

    return pkg;
}
shtp_pkg_def chk_ack_wr_cfg(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                        // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__WR) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                               // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_rd_cfg(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__RD;
    pkg.data[SHTP_IDX_ID]      = buf.seq_id;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_pkg_def chk_ack_rd_cfg(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                        // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__RD) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&pkg.data, &buf.data[SHTP_IDX_DATA], len);
                pkg.data_len = len;
                pkg.success  = 1;
                // user call structure decode.
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_on(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__ON;
    pkg.data[SHTP_IDX_ID]      = buf.seq_id;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_pkg_def chk_ack_on(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                        // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__ON) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                               // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_off(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__OFF;
    pkg.data[SHTP_IDX_ID]      = buf.seq_id;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_pkg_def chk_ack_off(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                         // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__OFF) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                                // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

// shtp_pkg_def gen_cmd_slp()
// {
// }
// shtp_pkg_def chk_ack_slp()
// {
// }

// shtp_pkg_def gen_cmd_deep_slp()
// {
// }
// shtp_pkg_def chk_ack_deep_slp()
// {
// }

// shtp_pkg_def gen_cmd_clk_sync_en()
// {
// }
// shtp_pkg_def chk_ack_clk_sync_en()
// {
// }

// shtp_pkg_def gen_cmd_clk_sync_dis()
// {
// }
// shtp_pkg_def chk_ack_clk_sync_dis()
// {
// }

// shtp_pkg_def gen_cmd_clk_sync()
// {
// }
// shtp_pkg_def chk_ack_clk_sync()
// {
// }

shtp_pkg_def gen_cmd_clk_wr() // replace with timestamp
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    shtp_time_def time;
    //------------------------------
    // parsing time to SHTP time
    //------------------------------

    uint16_t len = sizeof(time);

    pkg.data[SHTP_IDX_CMD] = SHTP_CMD__CLK_WR;
    pkg.data[SHTP_IDX_ID]  = 0;
    memcpy(&pkg.data[SHTP_IDX_LEN_LSB], &len, len);
    memcpy(&pkg.data[SHTP_IDX_DATA], &time, sizeof(time));
    pkg.data_len = 4 + len;
    pkg.success  = 1;

    return pkg;
}
shtp_pkg_def chk_ack_clk_wr(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                            // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__CLK_WR) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                                   // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_clk_rd(void)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__CLK_RD;
    pkg.data[SHTP_IDX_ID]      = 0;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_time_def chk_ack_clk_rd(const shtp_pkg_def buf) // replace with timestamp
{
    shtp_time_def time;
    memset(&time, 0, sizeof(time));

    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                            // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__CLK_RD) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&time.zone, &buf.data[SHTP_IDX_DATA], len);
                time.success = 1;
                // user call structure decode.
            }
        }
    }
    return time;
}

shtp_pkg_def gen_cmd_enum_num(void)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__ENUM_NUM;
    pkg.data[SHTP_IDX_ID]      = 0;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_enum_def chk_ack_enum_num(const shtp_pkg_def buf)
{
    shtp_enum_def tmp_enum;
    memset(&tmp_enum, 0, sizeof(tmp_enum));

    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                              // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__ENUM_NUM) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&tmp_enum.num, &buf.data[SHTP_IDX_DATA], len);
                tmp_enum.success = 1;
            }
        }
    }
    return tmp_enum;
}

shtp_pkg_def gen_cmd_enum_type(const shtp_enum_def tmp_enum)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__ENUM_TYPE;
    pkg.data[SHTP_IDX_ID]      = tmp_enum.seq_id;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_enum_def chk_ack_enum_type(const shtp_pkg_def buf)
{
    shtp_enum_def tmp_enum;
    memset(&tmp_enum, 0, sizeof(tmp_enum));

    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                               // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__ENUM_TYPE) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&tmp_enum.dev_id, &buf.data[SHTP_IDX_DATA], len);
                tmp_enum.success = 1;
                // user call structure decode.
            }
        }
    }
    return tmp_enum;
}

shtp_pkg_def gen_cmd_set_key(const shtp_auth_def auth)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    uint16_t chksum = 0;
    for (uint8_t i = 0; i < sizeof(auth.key); i++) {
        chksum += auth.key[i];
    }

    uint16_t len = sizeof(auth.key) + sizeof(chksum);

    pkg.data[SHTP_IDX_CMD] = SHTP_CMD__SET_KEY;
    pkg.data[SHTP_IDX_ID]  = 0;
    memcpy(&pkg.data[SHTP_IDX_LEN_LSB], &len, sizeof(len));                       // copy len
    memcpy(&pkg.data[SHTP_IDX_DATA], &auth.key, sizeof(auth.key));                // copy key
    memcpy(&pkg.data[SHTP_IDX_DATA + sizeof(auth.key)], &chksum, sizeof(chksum)); // copy check sum
    pkg.data_len = 4 + len;
    pkg.success  = 1;

    return pkg;
}
shtp_pkg_def chk_ack_set_key(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                             // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__SET_KEY) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                                    // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_clr_key(const shtp_auth_def auth)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    uint16_t len = sizeof(auth.key);

    pkg.data[SHTP_IDX_CMD] = SHTP_CMD__CLR_KEY;
    pkg.data[SHTP_IDX_ID]  = 0;
    memcpy(&pkg.data[SHTP_IDX_LEN_LSB], &len, sizeof(len));        // copy len
    memcpy(&pkg.data[SHTP_IDX_DATA], &auth.key, sizeof(auth.key)); // copy key
    pkg.data_len = 4 + sizeof(auth.key);
    pkg.success  = 1;

    return pkg;
}
shtp_pkg_def chk_ack_clr_key(shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                             // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__CLR_KEY) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                                    // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_req_access(const shtp_auth_def auth)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    uint16_t len = sizeof(auth.key);

    pkg.data[SHTP_IDX_CMD] = SHTP_CMD__REQ_ACCESS;
    pkg.data[SHTP_IDX_ID]  = 0;
    memcpy(&pkg.data[SHTP_IDX_LEN_LSB], &len, sizeof(len));        // copy len
    memcpy(&pkg.data[SHTP_IDX_DATA], &auth.key, sizeof(auth.key)); // copy key
    pkg.data_len = 4 + sizeof(auth.key);
    pkg.success  = 1;

    return pkg;
}
shtp_pkg_def chk_ack_req_access(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                                // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__REQ_ACCESS) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                                       // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_rd_model_id(void)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__RD_MODEL_ID;
    pkg.data[SHTP_IDX_ID]      = 0;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_model_def chk_ack_rd_model_id(const shtp_pkg_def buf)
{
    shtp_model_def model;
    memset(&model, 0, sizeof(model));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                                 // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__RD_MODEL_ID) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&model.model_id, &buf.data[SHTP_IDX_DATA], len);
                model.success = 1;
            }
        }
    }
    return model;
}

shtp_pkg_def gen_cmd_get_flash_next_wr_page(void)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__FLASH_LAST_WR_PAGE;
    pkg.data[SHTP_IDX_ID]      = 0;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_flash_def chk_ack_get_flash_next_wr_page(const shtp_pkg_def buf)
{
    shtp_flash_def flash;
    memset(&flash, 0, sizeof(flash));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                                        // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__FLASH_LAST_WR_PAGE) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&flash.wr_page_index, &buf.data[SHTP_IDX_DATA], len);
                flash.success = 1;
            }
        }
    }
    return flash;
}

// shtp_pkg_def gen_cmd_flash_wr_page()
// {
// }
// shtp_pkg_def chk_ack_flash_wr_page()
// {
// }

shtp_pkg_def gen_cmd_flash_rd_page(const shtp_flash_def flash)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    uint16_t len = sizeof(flash.rd_page_index);

    pkg.data[SHTP_IDX_CMD] = SHTP_CMD__FLASH_RD_PAGE;
    pkg.data[SHTP_IDX_ID]  = 0;
    memcpy(&pkg.data[SHTP_IDX_LEN_LSB], &len, sizeof(len));                              // copy len
    memcpy(&pkg.data[SHTP_IDX_DATA], &flash.rd_page_index, sizeof(flash.rd_page_index)); // copy key
    pkg.data_len = 4 + sizeof(flash.rd_page_index);
    pkg.success  = 1;

    return pkg;
}
shtp_flash_def chk_ack_flash_rd_page(const shtp_pkg_def buf)
{
    shtp_flash_def flash;
    memset(&flash, 0, sizeof(flash));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                                   // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__FLASH_RD_PAGE) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&flash.data, &buf.data[SHTP_IDX_DATA], len);
                flash.data_len = len;
                flash.success  = 1;
            }
        }
    }
    return flash;
}

shtp_pkg_def gen_cmd_get_flash_info(void)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__FLASH_PAGE_INFO;
    pkg.data[SHTP_IDX_ID]      = 0;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_flash_def chk_ack_get_flash_info(const shtp_pkg_def buf)
{
    shtp_flash_def flash;
    memset(&flash, 0, sizeof(flash));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                                     // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__FLASH_PAGE_INFO) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&flash.num_of_page, &buf.data[SHTP_IDX_DATA], len);
                flash.success = 1;
                // user call structure decode.
            }
        }
    }
    return flash;
}

shtp_pkg_def gen_cmd_get_random_key(void)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__RANDOM_KEY;
    pkg.data[SHTP_IDX_ID]      = 0;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_auth_def chk_ack_get_random_key(const shtp_pkg_def buf)
{
    shtp_auth_def auth;
    memset(&auth, 0, sizeof(auth));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                                // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__RANDOM_KEY) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&auth.key, &buf.data[SHTP_IDX_DATA], len);
                auth.success = 1;
                // user call structure decode.
            }
        }
    }
    return auth;
}

shtp_pkg_def gen_cmd_random_key_access(const shtp_auth_def auth)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    uint16_t len = sizeof(auth.key);

    pkg.data[SHTP_IDX_CMD] = SHTP_CMD__RANDOM_KEY_ACCESS;
    pkg.data[SHTP_IDX_ID]  = 0;
    memcpy(&pkg.data[SHTP_IDX_LEN_LSB], &len, sizeof(len));        // copy len
    memcpy(&pkg.data[SHTP_IDX_DATA], &auth.key, sizeof(auth.key)); // copy key
    pkg.data_len = 4 + len;
    pkg.success  = 1;

    return pkg;
}
shtp_pkg_def chk_ack_random_key_access(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                                       // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__RANDOM_KEY_ACCESS) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                                              // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_set_phone(const shtp_phone_def phone)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    uint16_t len = sizeof(phone.country) + 5; // phone use 40bit only

    pkg.data[SHTP_IDX_CMD] = SHTP_CMD__SET_PHONE;
    pkg.data[SHTP_IDX_ID]  = 0;
    memcpy(&pkg.data[SHTP_IDX_LEN_LSB], &len, sizeof(len));                   // copy len
    memcpy(&pkg.data[SHTP_IDX_DATA], &phone.country, sizeof(phone.country));  // copy country
    memcpy(&pkg.data[SHTP_IDX_DATA + sizeof(phone.country)], &phone.code, 5); // copy phone
    pkg.data_len = 4 + len;

    // check sum
    uint16_t chksum = 0;
    for (uint8_t i = 0; i < len; i++) {
        chksum += pkg.data[SHTP_IDX_DATA + i];
    }
    memcpy(&pkg.data[pkg.data_len], &chksum, sizeof(chksum)); // copy check sum
    pkg.data_len += sizeof(chksum);

    pkg.success = 1;

    return pkg;
}
shtp_pkg_def chk_ack_set_phone(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                               // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__SET_PHONE) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                                      // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}

shtp_pkg_def gen_cmd_get_phone(void)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__GET_PHONE;
    pkg.data[SHTP_IDX_ID]      = 0;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_phone_def chk_ack_get_phone(const shtp_pkg_def buf)
{
    shtp_phone_def phone;
    memset(&phone, 0, sizeof(phone));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                               // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__GET_PHONE) { // check cmd match
            uint16_t len;
            memcpy(&len, &buf.data[SHTP_IDX_LEN_LSB], sizeof(len));
            if (len) { // len non 0 is valid
                memcpy(&phone.country, &buf.data[SHTP_IDX_DATA], len);
                phone.success = 1;
                // user call structure decode.
            }
        }
    }
    return phone;
}

shtp_pkg_def gen_cmd_ignore_btn(void)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));

    pkg.data[SHTP_IDX_CMD]     = SHTP_CMD__PAIR_IGNORE_BTN;
    pkg.data[SHTP_IDX_ID]      = 0;
    pkg.data[SHTP_IDX_LEN_LSB] = 0;
    pkg.data[SHTP_IDX_LEN_MSB] = 0;
    pkg.data_len               = 4;
    pkg.success                = 1;

    return pkg;
}
shtp_pkg_def chk_ack_ignore_btn(const shtp_pkg_def buf)
{
    shtp_pkg_def pkg;
    memset(&pkg, 0, sizeof(pkg));
    if (buf.data[SHTP_IDX_CMD] & SHTP_CMD__ACK) {                                     // check ack flag
        if ((buf.data[SHTP_IDX_CMD] & ~SHTP_CMD__ACK) == SHTP_CMD__PAIR_IGNORE_BTN) { // check cmd match
            if (buf.data[SHTP_IDX_DATA]) {                                            // result 1 is pass
                pkg.success = 1;
            }
        }
    }
    return pkg;
}


//// decoder for connection & flash
//shtp_conn_pkg_def conn_and_flash_data_split(const shtp_pkg_def buf)
//{
//    shtp_conn_pkg_def pkg;
//    memset(&pkg, 0, sizeof(pkg));
//
//    // get cmd at header
//    pkg.cmd    = buf.data[SHTP_CONN_IDX_CMD];
//    pkg.seq_id = buf.data[SHTP_CONN_IDX_ID];
//    memcpy(&pkg.len, &buf.data[SHTP_CONN_IDX_LEN_LSB], sizeof(pkg.len));
//    memcpy(&pkg.time.DD, &buf.data[SHTP_CONN_IDX_TIME], 6); // get timestamp
//    memcpy(&pkg.data, &buf.data[SHTP_CONN_IDX_DATA], pkg.len);
//    // check from connection or flash
//    if (pkg.cmd == SHTP_CMD__FLASH_RPT) {
//        pkg.success = 1;
//    } else if (pkg.cmd == SHTP_CMD__CONN_RPT) {
//        pkg.success = 1;
//    } else {
//        pkg.success = 0;
//    }
//}
