
#include "App_CipherController.h"

const uint8_t cipherAdd[] = { 0x79, 0x6D, 0x56, 0x41, 0x38, 0x25 };
#define _MAC_SIZE_ (6)

uint8_t App_cipherWord_record = 0xE1;


// src limit N*6 + 1 byte
uint8_t App_cipherEnDeCode(uint8_t* ptr, uint8_t ptr_len, uint8_t cipherWord_begin, uint64_t mac)
{
    unsigned char cipher[_MAC_SIZE_];
    unsigned char cipherWord = cipherWord_begin;

    unsigned char MSB1, MSB2;

    // Step 1: Assign mac address to cipher
    memcpy(&cipher[0], &mac, _MAC_SIZE_);

    for (int i = 0; i < ((ptr_len / _MAC_SIZE_) + (((ptr_len % _MAC_SIZE_) > 0) ? 1 : 0)); i++) {
        MSB1 = (cipher[0] & 0x07) << 5;
        for (int j = 5; j >= 0; j--) {
            // Step 2: Shift bits
            MSB2      = (cipher[j] & 0x07) << 5;
            cipher[j] = (MSB1 + (cipher[j] >> 3)) & 0xFF;

            // Step 3: Add value
            cipher[j] = (cipher[j] + cipherAdd[5 - j]) & 0xFF;

            MSB1 = MSB2;
        }

        for (int j = 5; j >= 0; j--) {
            cipher[j] = cipher[j] ^ cipherWord;
        }

        for (int j = 0; (i * _MAC_SIZE_) + j < ptr_len && j < _MAC_SIZE_; j++) {
            // continue array
            *(ptr + i * _MAC_SIZE_ + j) = *(ptr + i * _MAC_SIZE_ + j) ^ cipher[5 - j];
        }

        cipherWord = (cipherWord + 3) & 0xFF;
        // UART_PRINTF("cipherWord = %02X\r\n", cipherWord);
    }

#if 0
    UART_PRINTF("after op : \r\n");
    for (uint8_t i = 0; i < ptr_len; i++) {
        UART_PRINTF("%02X_", *(ptr + i));
    }
    UART_PRINTF("\r\n\r\n");
#endif

    // sum as next start value
    uint8_t sum = 0;
    for (uint8_t i = 0; i < ptr_len; i++) {
        sum = sum + *(ptr + i);
    }
    App_cipherWord_record = (uint8_t)sum + 0x0A;

    // copy result back to array
    // memcpy(&data[23], &buff[0], ptr_len);
    // already in position

    return cipherWord_begin;
}
