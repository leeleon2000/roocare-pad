
#ifndef MBSN_ADV_DECODE_H_
#define MBSN_ADV_DECODE_H_

typedef unsigned char uint8_t;

#define CMD_INDEX (0)
#define DEV_ID_INDEX (1)
#define DEV_ID_LEN (2)
#define TIME_STAMP_INDEX (3)
#define TIME_STAMP_LEN (3)
#define SYS_RPT_INDEX (6)

#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"

#include "App_CipherController.h"

// struct DataStruct;

// put data after decode
typedef struct ScanResult {
    // result data
    uint8_t  success;
    uint8_t  MAC_addr[6];
    time_t   time_UTC; // UTC time stamp
    uint16_t time_ms;
    uint8_t  report;
    uint16_t  nationalCode; // international code
    uint64_t phoneCode;    // phone number
    uint8_t  alarm;        // last 7 day alarm
    float    skinTemp;     // temperature data
} ScanResult_1001_def;
#define DECODE_START_INDEX_1001 (3)
#define DECODE_LEN_1001 (13)
#define NATIONAL_CODE_INDEX_1001 (7)
#define NATIONAL_CODE_LEN_1001 (2)
#define PHONE_CODE_INDEX_1001 (9)
#define PHONE_CODE_LEN_1001 (5)
#define TEMP_INDEX_1001 (14)
#define DECODE_KEY_INDEX_1001 (16)

#ifdef __cplusplus
extern "C" {
#endif

int64_t decodeMac(const uint8_t* data, const int length);
ScanResult_1001_def decodeScan(const uint8_t* data, const int length, int64_t mac);
// Other function declarations here

#ifdef __cplusplus
}
#endif

// void decodeScanData(const uint8_t* data, int length, ScanCallback callback);
#endif
