

#ifndef MBSN_CONN_DECODE_H_
#define MBSN_CONN_DECODE_H_

#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

// conn_pkg byte index
#define SHTP_CONN_IDX_CMD 0
#define SHTP_CONN_IDX_ID 1
#define SHTP_CONN_IDX_LEN_LSB 2
#define SHTP_CONN_IDX_LEN_MSB 3
#define SHTP_CONN_IDX_TIME 4
#define SHTP_CONN_IDX_DATA 10

// byte index
#define SHTP_IDX_CMD 0
#define SHTP_IDX_ID 1
#define SHTP_IDX_LEN_LSB 2
#define SHTP_IDX_LEN_MSB 3
#define SHTP_IDX_DATA 4

// SHTP cmd list
#define SHTP_CMD__ACK (0x80)
#define SHTP_CMD__ACK_OK (1)

#define SHTP_CMD__WR (1)
#define SHTP_CMD__RD (2)
#define SHTP_CMD__RST (3)
#define SHTP_CMD__ON (4)
#define SHTP_CMD__OFF (5)
#define SHTP_CMD__SLP (6)
#define SHTP_CMD__DEEP_SLP (7)
#define SHTP_CMD__CLK_SYNC_EN (8)
#define SHTP_CMD__CLK_SYNC_DIS (9)
#define SHTP_CMD__CLK_SYNC (10)
#define SHTP_CMD__CLK_WR (11)
#define SHTP_CMD__CLK_RD (12)
#define SHTP_CMD__ADV_TX_EN (13)
#define SHTP_CMD__ADV_TX_DIS (14)
#define SHTP_CMD__ADV_RX_EN (15)
#define SHTP_CMD__ADV_RX_DIS (16)
#define SHTP_CMD__ADV_CONN_EN (17)
#define SHTP_CMD__ADV_CONN_DIS (18)
#define SHTP_CMD__CONN_EN (19)
#define SHTP_CMD__CONN_DIS (20)
#define SHTP_CMD__DEV_ACT (21)
#define SHTP_CMD__ENUM_NUM (22)
#define SHTP_CMD__ENUM_TYPE (23)
#define SHTP_CMD__SET_KEY (24)
#define SHTP_CMD__CLR_KEY (25)
#define SHTP_CMD__REQ_ACCESS (26)
#define SHTP_CMD__RD_MODEL_ID (27)
#define SHTP_CMD__FLASH_LAST_WR_PAGE (28)
#define SHTP_CMD__FLASH_WR_PAGE (29)
#define SHTP_CMD__FLASH_RD_PAGE (30)
#define SHTP_CMD__FLASH_ERASE_PAGE (31)
#define SHTP_CMD__FLASH_PAGE_INFO (32)
#define SHTP_CMD__RANDOM_KEY (33)
#define SHTP_CMD__RANDOM_KEY_ACCESS (34)
#define SHTP_CMD__OD_PULL_LV (35)
#define SHTP_CMD__OD_PULL_GET (36)
#define SHTP_CMD__SET_PHONE (37)
#define SHTP_CMD__GET_PHONE (38)
#define SHTP_CMD__DEV_SCAN_RESP_PAIR (39) // ADV
#define SHTP_CMD__DEV_SCAN_RESP_NOM (40)  // ADV
#define SHTP_CMD__PAIR_IGNORE_BTN (41)

#define SHTP_CMD__ADV_RPT (124) // ADV
#define SHTP_CMD__FLASH_RPT (125)
#define SHTP_CMD__CONN_RPT (126)

// Send cmd package with lenth
// APP -> Memsor device
typedef struct {
    uint8_t  success;
    uint8_t  seq_id;
    uint16_t data_len;
    uint8_t  data[256];
} shtp_pkg_def;

typedef struct {
    uint8_t  success;
    int8_t   zone; // -128~127
    uint16_t YYYY;
    uint8_t  MM;
    uint8_t  DD;
    uint8_t  hh;
    uint8_t  mm;
    uint8_t  ss;
    uint16_t ms;
} shtp_time_def;

typedef struct {
    // result
    uint8_t success;

    uint8_t  seq_id; // specific id
    uint8_t  num;
    uint16_t dev_id;
    uint32_t cfg1;
    uint32_t cfg2;
    uint8_t  rpt_len;
} shtp_enum_def;

typedef struct {
    // result
    uint8_t success;

    // key
    uint8_t key[3];
} shtp_auth_def;

typedef struct {
    // result
    uint8_t success;

    // model name
    uint16_t model_id;
} shtp_model_def;

typedef struct {
    // result
    uint8_t success;

    // flash page information
    uint32_t num_of_page;
    uint16_t page_size;

    // RW page index
    uint32_t wr_page_index;
    uint32_t rd_page_index;

    // buffer
    uint16_t data_len;
    uint8_t  data[256];
} shtp_flash_def;

typedef struct {
    // result
    uint8_t success;

    uint16_t country;
    uint64_t code;
} shtp_phone_def;

typedef struct {
    // result
    uint8_t       success;
    uint8_t       cmd; // received connection package type
    uint8_t       seq_id;
    uint16_t      len;
    shtp_time_def time;
    uint8_t       data[256];
} shtp_conn_pkg_def;


#ifdef __cplusplus
extern "C" {
#endif
extern shtp_pkg_def      gen_cmd_wr_cfg(const shtp_pkg_def buf);
extern shtp_pkg_def      chk_ack_wr_cfg(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_rd_cfg(const shtp_pkg_def buf);
extern shtp_pkg_def      chk_ack_rd_cfg(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_on(const shtp_pkg_def buf);
extern shtp_pkg_def      chk_ack_on(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_off(const shtp_pkg_def buf);
extern shtp_pkg_def      chk_ack_off(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_clk_wr(void);
extern shtp_pkg_def      chk_ack_clk_wr(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_clk_rd(void);
extern shtp_time_def     chk_ack_clk_rd(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_enum_num(void);
extern shtp_enum_def     chk_ack_enum_num(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_enum_type(const shtp_enum_def tmp_enum);
extern shtp_enum_def     chk_ack_enum_type(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_set_key(const shtp_auth_def auth);
extern shtp_pkg_def      chk_ack_set_key(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_clr_key(const shtp_auth_def auth);
extern shtp_pkg_def      chk_ack_clr_key(shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_req_access(const shtp_auth_def auth);
extern shtp_pkg_def      chk_ack_req_access(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_rd_model_id(void);
extern shtp_model_def    chk_ack_rd_model_id(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_get_flash_next_wr_page(void);
extern shtp_flash_def    chk_ack_get_flash_next_wr_page(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_flash_rd_page(const shtp_flash_def flash);
extern shtp_flash_def    chk_ack_flash_rd_page(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_get_flash_info(void);
extern shtp_flash_def    chk_ack_get_flash_info(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_get_random_key(void);
extern shtp_auth_def     chk_ack_get_random_key(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_random_key_access(const shtp_auth_def auth);
extern shtp_pkg_def      chk_ack_random_key_access(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_set_phone(const shtp_phone_def phone);
extern shtp_pkg_def      chk_ack_set_phone(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_get_phone(void);
extern shtp_phone_def    chk_ack_get_phone(const shtp_pkg_def buf);
extern shtp_pkg_def      gen_cmd_ignore_btn(void);
extern shtp_pkg_def      chk_ack_ignore_btn(const shtp_pkg_def buf);
extern shtp_conn_pkg_def conn_and_flash_data_split(const shtp_pkg_def buf);

#ifdef __cplusplus
}
#endif


#endif
