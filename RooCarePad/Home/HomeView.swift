//
//  ContentView.swift
//  RooCare
//
//  Created by Leon Lee on 2020/7/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

let deviceListViewModel = DeviceListViewModel()

struct HomeView: View {
    @ObservedObject var viewModel: HomeViewModel
    let scanViewModel = ScanViewModel()
    let notificationViewModel = NotificationListViewModel()
    
    var body: some View {
        if(viewModel.isFirstTimeAgreement){
            FirstLaunchAgreementView(viewModel: viewModel)
        }
        else if(viewModel.isFristTimeLaunch){
            //if(settingManager.isFirstTimeLaunch){
            FirstLaunchView(viewModel: viewModel)
        } else{
            TabView(selection: $viewModel.selectedTab) {
                ScanView(viewModel: scanViewModel).tabItem {
                    Text("Home".localized())
                    Image("ic_home").renderingMode(.template)                
                }.tag(HomeTab.home)
                DeviceListView(viewModel: deviceListViewModel).tabItem {
                    Text("Devices".localized())
                    Image("ic_device_list").renderingMode(.template)
                }.tag(HomeTab.device)
                NotificationView(viewModel: notificationViewModel).tabItem {
                    Text("Notification".localized())
                    Image("ic_bell").renderingMode(.template)
                }.tag(HomeTab.notification)
                SettingView(viewModel: SettingListViewModel()).tabItem {
                    Text("Settings".localized())
                    Image("ic_setting").renderingMode(.template)
                }.tag(HomeTab.setting)
            }.onDisappear{
                self.viewModel.stop()
            }.onAppear{
                UITableView.appearance().separatorStyle = .none
                self.viewModel.start()
            }
        }
    }
}
