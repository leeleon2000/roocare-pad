//
//  FirstLaunchView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/26.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct FirstLaunchAgreementView: View {
    @ObservedObject var viewModel: HomeViewModel
    
    var body: some View {
        VStack{
            Text("PRIVACY & DISCLAIMER")
                .foregroundColor(UIColor.MainColor.color)
                .font(.system(size: 24))
                .padding()
                .padding(.top, 20)
            Text("We never use or share your personal information without your permission.")
                .padding()
            Text("We are not a substitute for your doctor, and our guidance is not appropriate for everyone.")
                .padding()
            Text("I agree to the following:")
            Button("* Terms & Conditions", action: {
                UIApplication.shared.open(URL.init(string: "https://www.roocare.jp/terms-of-use")!)
            }).padding()
            Button("* Privacy Policy", action: {
                UIApplication.shared.open(URL.init(string: "https://www.roocare.jp/privacy-policy")!)
            }).padding()
            Button("* Medical Disclaimer", action: {
                UIApplication.shared.open(URL.init(string: "https://www.roocare.jp/disclaimer")!)
            }).padding()
            
            Button("Agree", action:{
                settingManager.isFirstTimeAgreement = false
                viewModel.isFirstTimeAgreement = false
            }).font(.system(size: 24))
            .padding([.leading, .trailing], 60)
            .defaultSolidOutline()
            .padding()

            Spacer()
        }
    }
}

struct FirstLaunchView: View {
    
    @ObservedObject var viewModel: HomeViewModel
    
    var body: some View {
        VStack{
            Button("Personal Use"){
                viewModel.isFristTimeLaunch = false
                homeTabManager.currentTab.accept(.device)
            }.padding(4)
            .defaultOutline()
            .padding(8)
            
            Button("Corporation Use"){
                settingManager.isFirstTimeGuide = false
                viewModel.isFristTimeLaunch = false
                homeTabManager.isShowLicense = true
                homeTabManager.currentTab.accept(.setting)

            }.padding(4)
            .defaultOutline()
            .padding(8)
        }
    }
}

struct FirstLaunchView_Previews: PreviewProvider {
    static var previews: some View {
        FirstLaunchAgreementView(viewModel: HomeViewModel())
    }
}
