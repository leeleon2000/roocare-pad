//
//  SettingViewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import UIKit

struct NotificationListCellViewModel: Identifiable {
    var id = UUID()
    let title: String
    let temp: Double
    let date: String
    let deviceID: Int64
}

class NotificationListViewModel: BaseViewModel, ObservableObject{
    @Published var list: [NotificationListCellViewModel] = []
    
    var deviceID: Int64? = nil
    
    init(deviceID: Int64? = nil) {
        self.deviceID = deviceID
    }
    
    override func onStart() {
        notificationManager.list.subscribe(onNext:{
            self.list = $0
                .filter{ self.deviceID == nil || $0.deviceID == self.deviceID}
                .map{ $0.toCellViewModel()                
            }
        }).disposed(by: disposeBag)
        //notificationManager.saveTest()
    }
}

extension NotificationRecord{
    func toCellViewModel() -> NotificationListCellViewModel {
        return NotificationListCellViewModel(title: self.title, temp: self.temp, date: self.date.string(withFormat: "yyyy-MM-dd HH:mm:ss"), deviceID: self.deviceID ?? -1)
    }
}
