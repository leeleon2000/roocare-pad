//
//  NotificationManager.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/14.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwifterSwift

let notificationManager = NotificationManager()
class NotificationManager {
    let list = BehaviorRelay<[NotificationRecord]>(value: [])
    
    init() {
        list.accept(PadDataBase.instance.loadNotificationList())
    }
    
    func saveTest(){
        self.save(record: NotificationRecord(title: "test", date: Date()))
    }
    
    func save(record: NotificationRecord) {
        PadDataBase.instance.saveNotification(notification: record)
        list.accept(PadDataBase.instance.loadNotificationList())

//        var oldList = list.value
//        oldList.insert(record, at: 0)
//        self.list.accept(oldList)
    }
}
