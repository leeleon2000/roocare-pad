//
//  SettingView.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/5.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct NotificationListCellView: View {
    let viewModel: NotificationListCellViewModel
    var body: some View {
        HStack{
            Text(viewModel.date)
            Spacer()
            Text(viewModel.title)
            Spacer()
            Text(unitManager.getDegreeText(value: viewModel.temp))
        }
    }
}


struct NotificationView: View {
    @ObservedObject var viewModel: NotificationListViewModel
    
    var body: some View {
        NavigationView{
            List{
                ForEach(0..<viewModel.list.count, id: \.self){ index in
                    let data = viewModel.list[index]
                    
                    NavigationLink(destination: HistoryView(viewModel: HistoryViewModel(cellViewModel: DeviceListCellViewModel(record: PadDataBase.instance.findDeviceRecord(deviceID: data.deviceID)!), selection: ""))){
                        NotificationListCellView(viewModel: data)
                    }
                    .padding(4)
                    .defaultOutline()
                    .padding(10)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                    .listRowInsets(EdgeInsets())
                    .background(Color.white)
                }
            }.navigationBarTitle("Notification")
        }.onAppear{
            viewModel.start()
            UITableView.appearance().separatorStyle = .none
        }.onDisappear{
            viewModel.stop()
        }
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView(viewModel: NotificationListViewModel())
    }
}
