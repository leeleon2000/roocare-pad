//
//  SettingDetailView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct NotificationDetailView: View {
    let viewModel: NotificationListCellViewModel
    var body: some View {
        HStack{
            Text(viewModel.date)
            Text(viewModel.title)
        }
    }
}
