//
//  HomeViewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/10.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

let homeTabManager = HomeTabManager()

enum HomeTab: Hashable {
    case home
    case device
    case notification
    case setting
}

class HomeTabManager{
    let currentTab = BehaviorRelay.init(value: HomeTab.home)
    var isShowLicense = false
}

class HomeViewModel: BaseViewModel, ObservableObject{
    @Published var selectedTab: HomeTab = .home{
        willSet(value){
            if(homeTabManager.currentTab.value != value){
                homeTabManager.currentTab.accept(value)
            }
        }
    }
        
    @Published var isFristTimeLaunch = settingManager.isFirstTimeLaunch{
        willSet(value){
            settingManager.isFirstTimeLaunch = value
        }
    }
    
    @Published var isFirstTimeAgreement = settingManager.isFirstTimeAgreement{
        willSet(value){
            settingManager.isFirstTimeAgreement = value
        }
    }
    
    override func start()  {
        homeTabManager.currentTab.subscribe(onNext:{
            if(self.selectedTab != $0){
                self.selectedTab = $0
            }
        }).disposed(by: disposeBag)
    }
}
