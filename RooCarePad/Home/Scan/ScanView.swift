//
//  ScanView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/7/25.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI


class ViewModel: ObservableObject{
    @Published var count = 0
    func start(){
        count = 44
    }
}

struct ScanView: View {
    @ObservedObject var viewModel: ScanViewModel
    
    var body: some View {
        NavigationView{
            VStack{
                QGrid(viewModel.scanItems, columns: 1, vSpacing: 10, vPadding: 10, content: { data in
                    ScanCellView(viewModel: data)
                }).onAppear{
                    print("start viewmodel \(getPointer(obj: self.viewModel))")                  
                    self.viewModel.start()
                }.onDisappear{
                    self.viewModel.stop()
                }
                .navigationBarTitle("Home".localized())
            }
        }.buttonStyle(PlainButtonStyle())
        .navigationViewStyle(StackNavigationViewStyle())
        .onAppear(){
        }
    }
}


struct BellToggleStyle: ToggleStyle{
    func makeBody(configuration: Self.Configuration) -> some View {
        Image(configuration.isOn ? "ic_bell_on" : "ic_bell_off")
            .resizable()
            .frame(width: 24, height: 24)
            .onTapGesture {
                configuration.isOn.toggle()
            }
    }
}

struct ScanCellView: View {
    @ObservedObject var viewModel: ScanItemViewModel
    var body: some View {
        HStack() {
            Text(viewModel.displayName).lineLimit(1).frame(minWidth: 0, maxWidth: .infinity).onTapGesture {
                homeTabManager.currentTab.accept(.device)
                deviceListViewModel.selection = viewModel.id
            }
            Toggle("", isOn: $viewModel.isBellOn).toggleStyle(BellToggleStyle())
            Text("\(viewModel.temperature)")
        }
        .padding(25)
        .overlay(
            RoundedRectangle(cornerRadius: 20)
                .stroke(viewModel.color.color, style: StrokeStyle(lineWidth: 3))
                .padding(1)
        )
    }
}

struct ScanView_Previews: PreviewProvider {
    static var previews: some View {
        ScanView(viewModel: ScanViewModel())
    }
}
