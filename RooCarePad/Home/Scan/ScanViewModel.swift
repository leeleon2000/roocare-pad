//
//  ScanViewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/7/26.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import AVFoundation
import RxSwift
import SwifterSwift

public class ScanViewModel: BaseViewModel, ObservableObject {
    @Published var scanItems: [ScanItemViewModel] = []
    @Published var size = 0
    let manager = deviceManager
    
    override public func onStart() {
        print("ScanViewModel start")
        super.start()
        manager.startScanData()
        deviceManager.scanDataList.subscribe(onNext:{ list in
            self.scanItems = list.map { (data) in
                data.value.toScanItemViewModel()
            }
        }).disposed(by: disposeBag)
        
        Observable<Int>.interval(.seconds(3), scheduler: MainScheduler.instance).subscribe(onNext: {  _ in
            if(self.scanItems.any(matching: {
                $0.isBellOn && settingManager.shouldShowHighAlert(temperature: $0.tempValue)
            })){
                AudioServicesPlaySystemSound(SystemSoundID(1005))
            }
        }).disposed(by: disposeBag)
    }
    
    public override func stop() {
        super.stop()
        print("ScanViewModel stop")
    }
}

class ScanItemViewModel: BaseViewModel, Identifiable, ObservableObject {
    
    init(name: String, id: UUID, tempValue: Float, phone: String) {
        self.name = name
        self.id = id
        self.tempValue = tempValue
        
        if(phone != "0"){
            self.phone = phone
        }else{
            self.phone = ""
        }
        
        self.isBellOn = settingManager.isBellOn[id] ?? true
    }
    
    @Published var isBellOn: Bool = true{
        willSet(value){
            settingManager.isBellOn[id] = value
        }
    }
    let name: String
    let phone: String
    let id: UUID
    let tempValue: Float
    
    var displayName: String{
        get{
            if(!name.isEmpty){
                return name
            }else if(!phone.isEmpty){
                return phone
            }else{
                return "no name"
            }
        }
    }
    
    var temperature: String {
        get{
//            if(tempValue <= 30){
//                return "Low"
//            }
            return unitManager.getDegreeText(value: Double(tempValue))
        }
    }
    
    var color: UIColor{
        get{
            if(settingManager.shouldShowHighAlert(temperature: self.tempValue)){
                return UIColor.red
            }
            return UIColor.MainColor
        }
    }
}

extension DeviceScanData{
    func toScanItemViewModel() -> ScanItemViewModel{
        let uuid = scanDevice.peripheral.identifier        
        return ScanItemViewModel(name: deviceManager.uuidDeviceMap[uuid.uuidString]?.name ?? "",
                                 id: uuid,
                                 tempValue: self.decodedData?.skinTemp ?? 0.0,
                                 phone: String(self.decodedData?.phoneCode ?? 0)
                                 )
    }
}
