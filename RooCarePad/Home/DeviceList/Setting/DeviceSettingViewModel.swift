//
//  DeviceSettingViewModel.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/7.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

class DeviceSettingViewModel: BaseViewModel, ObservableObject{
    @Published var isShowBindAlert: Bool = false
    @Published var boundStatus = ""
    
    let cellViewModel: DeviceListCellViewModel
    var textName: String = ""
    var phone = ""
    var displayPhone: String{
      //  if(phone.isEmpty){
            return "Verify Phone No.".localized()
      //  }else{
      //      return phone
      //  }
    }
    let scanViewModel: ScanDeviceViewModel

    init(cellViewModel: DeviceListCellViewModel) {
        self.cellViewModel = cellViewModel
        self.textName = cellViewModel.record.name
        self.phone = cellViewModel.record.phone
        if(cellViewModel.record.mac.isEmpty){
            self.boundStatus = "Not bound to device"
        }else{
            self.boundStatus = "Bound to Device"
        }
        self.scanViewModel = ScanDeviceViewModel(record: self.cellViewModel.record)
    }
    
    func save(){
        var newDevice = cellViewModel.record
        newDevice.name = self.textName
        PadDataBase.instance.saveDevice(device: newDevice)
    }
    
    func deleteDevice(){
        PadDataBase.instance.deleteDevice(device: cellViewModel.record)
    }
    
    func showBindAlert(){
        self.isShowBindAlert = true
    }
}
