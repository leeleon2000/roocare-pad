//
//  SetPhoneVIewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/18.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

class SetPhoneViewModel: BaseViewModel, ObservableObject{
    
    internal init(record: DeviceRecord) {
        self.record = record
    }
    
    @Published var phone: String = ""
    @Published var code: String = ""
    @Published var isLoading: Bool = false
        
    let record: DeviceRecord

    var lastCode: Int = 0
    func sendCode(){
        lastCode = Int.random(in: 0...999999)
        let url = URL(string: "http://s2415372.synology.me/index_sms.php?PHONE_NUM=\(self.phone)&DATA=Your LifeTemp Verification code is : \(lastCode)".urlEncoded)!
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
        }
        task.resume()
    }
    
    func clickSave(complete: @escaping () -> ()){
        var cmd = gen_cmd_set_phone(shtp_phone_def(success: 0, country: 0, code: UInt64(phone) ?? 0))
        let dataBuffer = [UInt8](UnsafeBufferPointer(start: &cmd.data.0, count: MemoryLayout.size(ofValue: cmd.data_len)))
        let data = Data(dataBuffer)
        let list = deviceManager.scanDataList.value
        guard let device = list.first(where: {
            $0.key == record.uuid
        }) else {
            return
        }
        
        if(Int(lastCode) ?? -1 != lastCode){
            return
        }
        
        isLoading = true
        let peripheral = device.value.scanDevice.peripheral
        peripheral.establishConnection().flatMap {_ in
            peripheral.writeValue(data, for: DeviceCharacteristic.WRITE, type: .withoutResponse)
        }.subscribe(onNext:{
            print($0)
            var newDevice = self.record
            newDevice.phone = self.phone
            PadDataBase.instance.saveDevice(device: newDevice)

            complete()
        }, onError: {
            print($0)
            complete()
        }).disposed(by: disposeBag)
    }
}
