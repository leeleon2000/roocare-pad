//
//  ScanDeviceViewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/19.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import RxRelay

class ScanDeviceViewModel: BaseViewModel, ObservableObject{
    @Published var text: String = "Scanning...."
    @Published var enableSave = false
    
    let record: DeviceRecord
    var deviceToSave: ConnectScanData?
    let lastScannedDevice: BehaviorRelay<ConnectScanData?> = BehaviorRelay(value: nil)
    
    init(record: DeviceRecord) {
        self.record = record
    }
    
    override func start() {
        deviceManager.scanConnectList.subscribe(onNext:{ list in
            if let largest = list.max { (d1, d2) in
                d1.value.scanDevice.rssi.doubleValue > d2.value.scanDevice.rssi.doubleValue
                } {
                self.enableSave = true
                self.lastScannedDevice.accept(largest.value)
                self.text = "Scaned Device Mac: \(largest.value.mac)"
                self.deviceToSave = largest.value
            }
            }).disposed(by: disposeBag)
    }
    
    override func stop(){
        super.stop()        
    }
    
    func save() {
        guard let device = deviceToSave else {
            return
        }
        var newDevice = record
        newDevice.mac = device.mac
        newDevice.uuid = device.scanDevice.peripheral.identifier.uuidString
        PadDataBase.instance.saveDevice(device: newDevice)
        self.text = "Scanning...."
    }
}
