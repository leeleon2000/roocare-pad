//
//  SetPhoneView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/18.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import SwiftUI

struct SetPhoneView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var viewModel: SetPhoneViewModel
    var body: some View {
        LoadingView(isShowing: $viewModel.isLoading){
            VStack{
                HStack{
                    TextField("phone", text: $viewModel.phone)
                        .keyboardType(.phonePad)
                        .defaultOutline()
                    Button("Send code", action: {
                        viewModel.sendCode()
                    })
                }
                
                HStack{
                    TextField("Enter 6 digit code", text: $viewModel.code)
                        .defaultOutline()
                }
                
                Button(action: {
                    viewModel.clickSave(){
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }){
                    Text("Confirm").frame(minWidth: 0,maxWidth: .infinity)
                }.defaultSolidOutline().padding(.top, 4)
                
                Spacer()
            }.padding()
            .navigationBarTitle("Verify Phone No.")
            .onDisappear{
                viewModel.stop()
            }
        }
    }
}
