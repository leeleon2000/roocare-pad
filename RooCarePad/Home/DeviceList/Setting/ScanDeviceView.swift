//
//  ScanDevice.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/19.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct ScanDeviceView: View {
    @Binding var showModal: Bool
    @ObservedObject var viewModel: ScanDeviceViewModel
    var body: some View {
            VStack {
                Text(self.viewModel.text)
                Button("Save", action: {
                    self.viewModel.save()
                    self.showModal.toggle()
                }).disabled(!viewModel.enableSave).padding()
            }.onAppear{
                self.viewModel.start()
            }.onDisappear{
                self.viewModel.stop()
            }
    }
}

