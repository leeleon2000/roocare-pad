//
//  DeviceSettingView.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/7.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct DeviceSettingView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var viewModel: DeviceSettingViewModel
    
    var body: some View {
        VStack{
            Text("Thermometer")
                .font(.system(size: 24))
                .foregroundColor(UIColor.MainColor.color)
            //Text("Model: ")
            Text("UUID: \(viewModel.cellViewModel.record.uuid)")
            
            Text(self.viewModel.boundStatus)
                .padding(.leading, 16.0)
            
            Button("Bind", action: {
                self.viewModel.showBindAlert()
            }).padding().sheet(isPresented: $viewModel.isShowBindAlert){
                ScanDeviceView(showModal: self.$viewModel.isShowBindAlert, viewModel: self.viewModel.scanViewModel)
            }
            
            TextField("name", text: $viewModel.textName)
                .defaultOutline()
            NavigationLink(
                destination: SetPhoneView(viewModel: SetPhoneViewModel(record: viewModel.cellViewModel.record))){
                Text(viewModel.displayPhone).defaultOutline()
            }
            Button(action: {
                viewModel.save()
                self.presentationMode.wrappedValue.dismiss()
            }){
                Text("Confirm").frame(minWidth: 0,maxWidth: .infinity)
            }.defaultSolidOutline().padding(.top, 4)
            Button(action: {
                self.viewModel.deleteDevice()
                self.presentationMode.wrappedValue.dismiss()
            }){
                Text("Delete Device").frame(minWidth: 0,maxWidth: .infinity)
            }.defaultSolidOutline().padding(.top, 8)
            Spacer()
        }
        .padding()
        .navigationBarTitle("Settings")
    }
}
