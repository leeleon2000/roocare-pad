//
//  DeviceListViewModel.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/6.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

struct DeviceListCellViewModel: Identifiable, Hashable {
    
    static func == (lhs: DeviceListCellViewModel, rhs: DeviceListCellViewModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    internal init(id: UUID = UUID(), record: DeviceRecord, showScan: Bool = false) {
        self.id = id
        self.record = record
        self.showScan = showScan
    }
    
    var id = UUID()
    let record: DeviceRecord
    var showScan: Bool
    
}

class DeviceListViewModel: BaseViewModel, ObservableObject{
    @Published var isShowNewDeviceAlert: Bool = false
    @Published var newDeviceName = "Set Device Name".localized()
    @Published var deivceList: [DeviceListCellViewModel] = []
    @Published var selection: UUID! = UUID()    
    @Published var isShowBindAlert: Bool = false
   
    lazy var save = {
        let record = DeviceRecord(phone: "", name: self.newDeviceName, mac: "")
        self.selectRecord = PadDataBase.instance.saveDevice(device: record)
        self.selection = UUID(uuidString: self.selectRecord!.uuid)
        self.isShowBindAlert = true
    }
    
    override func start() {
        loadDeviceList()
        PadDataBase.instance.deviceList.map{
            $0.map{ DeviceListCellViewModel(record: $0) }
        }.subscribe(onNext:{
            self.deivceList = $0            
        }).disposed(by: disposeBag)
    }
    
    func loadDeviceList(){
        PadDataBase.instance.loadDeviceList()
    }
                
    func showDeviceAlert()  {
        self.isShowNewDeviceAlert = true
    }
    
    var selectRecord: DeviceRecord?

}
