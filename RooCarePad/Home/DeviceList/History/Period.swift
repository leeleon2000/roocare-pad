//
//  Period.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/11.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import SwiftDate

struct Period {
    let name: String
    let count: Int
    let dateToString: (Date) -> String
    let loadData: (_ deviceID: Int64) -> [BodyTemperatureRecord]
    
    static let list = [day, week, month]
     
    private static let day = Period(name: "day", count: 24, dateToString: {
        $0.string(withFormat: "HH:00")
    }, loadData: { deviceID in
        var start = Date()
        var end = Date()
        var dbData = PadDataBase.instance.loadTemperature(start: start, end: end, dateFormat: "%Y-%m-%d %H", deviceID: deviceID)
        
        var totalHours: Int
        if let first = dbData.first?.date, let last = dbData.last?.date{
            totalHours = (first - last).hour ?? 0
        }else{
            totalHours = 0
        }
        totalHours += 40
    
        var result: [BodyTemperatureRecord] = (0...totalHours).map{ d in
            let indexDay = Date() - totalHours.hours + d.hours
            var result = BodyTemperatureRecord(id: 0, deviceID: 0, value: 0.0, date: indexDay)
            let found = (dbData.first(where: {
                $0.date.compare(.isSameDay(indexDay)) && $0.date.hour == indexDay.date.hour
            }))
            
            if(found != nil){
                return found!
            }
            return result
        }
        return result
    })
    
    private static let week = Period(name: "week", count: 8, dateToString: {
        $0.weekdayName(.default)
    }, loadData: { deviceID in
        var start = Date()
        var end = Date()
        var dbData = PadDataBase.instance.loadTemperature(start: start, end: end, dateFormat: "%Y-%m-%d %H", deviceID: deviceID)
        
        var totalDays: Int
        if let first = dbData.first?.date, let last = dbData.last?.date{
            totalDays = (first - last).day ?? 0
        }else{
            totalDays = 0
        }
        totalDays += 40
    
        var result: [BodyTemperatureRecord] = (0...totalDays).map{ d in
            let indexDay = Date() - totalDays.days + d.days
            var result = BodyTemperatureRecord(id: 0, deviceID: 0, value: 0.0, date: indexDay)
            let found = (dbData.first(where: {
                $0.date.compare(.isSameDay(indexDay))
            }))
            
            if(found != nil){
                return found!
            }
            return result
        }
        return result
    })
    
    private static let month = Period(name: "month", count: 24, dateToString: {
        $0.string(withFormat: "MM/dd")
    }, loadData: { deviceID in
        var start = Date()
        var end = Date()
        var dbData = PadDataBase.instance.loadTemperature(start: start, end: end, dateFormat: "%Y-%m-%d %H", deviceID: deviceID)
        
        var totalDays: Int
        if let first = dbData.first?.date, let last = dbData.last?.date{
            totalDays = (first - last).day ?? 0
        }else{
            totalDays = 0
        }
        totalDays += 40
        
        var result: [BodyTemperatureRecord] = (0...totalDays).map{ d in
            let indexDay = Date() - totalDays.days + d.days
            var result = BodyTemperatureRecord(id: 0, deviceID: 0, value: 0.0, date: indexDay)
            let found = (dbData.first(where: {
                $0.date.compare(.isSameDay(indexDay))
            }))
            
            if(found != nil){
                return found!
            }
            return result
        }
        return result
    })
    
    func convertToBarModel(_ data: BodyTemperatureRecord)-> BarModel {
        return BarModel(date: data.date, value: data.value, dateText: dateToString(data.date))
    }
}
