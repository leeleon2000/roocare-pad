//
//  HistoryViewModel.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/7.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import SwiftDate
import RxSwift
import RxCocoa
import Combine

struct BindAlertModel: Identifiable{
    let id = UUID()
    let message = "device name"
}

struct TemperatureModel: Identifiable{
    let id = UUID()
    let high: Double
    let low: Double
    let avg: Double
}

class HistoryViewModel: BaseViewModel, ObservableObject{
    
    @Published var showScan: Bool = false
    @Published var temperatureModel = TemperatureModel(high: 0.0, low: 0.0, avg: 0.0)
    
    let periodIndex = BehaviorRelay(value: 0)
    lazy var period: Observable<Period> = periodIndex.map{
        let p = Period.list[$0]
        self.chartViewModel.period = p
        return p
    }
    
    
    let cellViewModel: DeviceListCellViewModel
    let chartViewModel = ChartViewModel()
    
    init(cellViewModel: DeviceListCellViewModel, selection: String) {
        self.cellViewModel = cellViewModel
    }
    
    override func start() {
        period.subscribe(onNext:{ p in
            var high = 0.0
            var low = 100.0
            var sum = 0.0
            var count = 0
            self.chartViewModel.historyData = p.loadData(self.cellViewModel.record.id!).map{
                let temperature = $0.value
                
                if(temperature != 0){
                    if(high < temperature){
                        high = temperature
                    }
                    
                    if(low > temperature){
                        low = temperature
                    }
                    sum += temperature
                    count += 1
                }
                return p.convertToBarModel($0)
            }
            if(count > 0){
                self.temperatureModel = TemperatureModel(high: high, low: low, avg: sum / Double(count))
            }
        }).disposed(by: disposeBag)
        
        //saveTest()
    }
    
    func saveTest(){
        PadDataBase.instance.saveTemperature(temperature: BodyTemperatureRecord(deviceID: 1, value: Double.random(in: 20...39), date: Date()))
    }
}
