//
//  ChartView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/8/9.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Charts
import SwifterSwift
import SwiftUI

class ValueFormatter: IndexAxisValueFormatter{
        
    override func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        super.stringForValue(value, axis: axis)
    }
    
}

class ChartMarker: MarkerView {
    var text = ""

    override func refreshContent(entry: ChartDataEntry, highlight: Highlight) {
        super.refreshContent(entry: entry, highlight: highlight)
        text =
            unitManager.getDegreeText(value: entry.y) +
            "\n\((entry.data as! Date).string(withFormat: "yyyy-MM-dd HH:00"))"
    }

    override func draw(context: CGContext, point: CGPoint) {
        super.draw(context: context, point: point)

        var drawAttributes = [NSAttributedString.Key : Any]()
        drawAttributes[.font] = UIFont.systemFont(ofSize: 15)
        drawAttributes[.foregroundColor] = UIColor.white
        drawAttributes[.backgroundColor] = UIColor.darkGray

        self.bounds.size = (" \(text) " as NSString).size(withAttributes: drawAttributes)
        self.offset = CGPoint(x: 0, y: -self.bounds.size.height - 2)

        let offset = self.offsetForDrawing(atPoint: point)

        drawText(text: " \(text) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y), size: self.bounds.size), withAttributes: drawAttributes)
    }

    func drawText(text: NSString, rect: CGRect, withAttributes attributes: [NSAttributedString.Key : Any]? = nil) {
        let size = text.size(withAttributes: attributes)
        let centeredRect = CGRect(x: rect.origin.x + (rect.size.width - size.width) / 2.0, y: rect.origin.y + (rect.size.height - size.height) / 2.0, width: size.width, height: size.height)
        text.draw(in: centeredRect, withAttributes: attributes)
    }
}

struct ChartView : UIViewRepresentable {
    //Bar chart accepts data as array of BarChartDataEntry objects
    @ObservedObject var viewModel: ChartViewModel
    // this func is required to conform to UIViewRepresentable protocol
    func makeUIView(context: Context) -> BarChartView {
        //crate new chart
        let chart = BarChartView()
        chart.legend.enabled = false
        chart.data = addData(chart: chart)
        chart.pinchZoomEnabled = false
        chart.doubleTapToZoomEnabled = false
        chart.scaleXEnabled = false
        chart.scaleYEnabled = false
        chart.xAxis.granularity = 1.0
        chart.xAxis.labelPosition = .bottom
        chart.xAxis.centerAxisLabelsEnabled = false
        chart.xAxis.drawGridLinesEnabled = false
        chart.xAxis.valueFormatter = viewModel.valueFormatter
        chart.leftAxis.drawGridLinesEnabled = false
        chart.leftAxis.drawLabelsEnabled = false
        chart.rightAxis.drawGridLinesEnabled = false
        chart.leftAxis.axisMinimum = 0
        chart.drawMarkers = true
        let marker = ChartMarker()
        marker.chartView = chart
        chart.marker = marker
        return chart
    }
    
    // this func is required to conform to UIViewRepresentable protocol
    func updateUIView(_ chart: BarChartView, context: Context) {
        //when data changes chartd.data update is required
        chart.data = addData(chart: chart)
    }
    
    func addData(chart: BarChartView) -> BarChartData{
        let data = BarChartData()
        //BarChartDataSet is an object that contains information about your data, styling and more
        
        var min = Double.greatestFiniteMagnitude
        var max = 0.0
        
        let dataSet = BarChartDataSet(entries: viewModel.historyData.enumerated().map({ index, item in
            if(min > index.double){
                min = index.double
            }
            
            if(max < index.double){
                max = index.double
            }
            return BarChartDataEntry(x: index.double, y: unitManager.getDegreeValue(value: item.value.float).double, data: item.date)
        }))
        
        if(max != 0.0){
            chart.xAxis.axisMinimum = 0
            chart.xAxis.axisMaximum = max
            chart.setVisibleXRangeMinimum(viewModel.period.count.double)
            chart.setVisibleXRangeMaximum(viewModel.period.count.double)
            chart.moveViewToX(max)
            print("set chart max count \(viewModel.period.count.double)")
        }
        
        viewModel.valueFormatter.values = viewModel.historyData.map{ $0.dateText }
        // change bars color to green
        dataSet.colors = [UIColor.MainColor]
        //change data label
        data.addDataSet(dataSet)
        data.barWidth = 0.9
        data.setDrawValues(false)
        return data
    }
    typealias UIViewType = BarChartView
}


struct ChartView_Previews: PreviewProvider {
    static var previews: some View {
        ChartView(viewModel: ChartViewModel())
    }
}
