//
//  ChartViewModel.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/11.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

struct BarModel {
    let date: Date
    let value: Double
    let dateText: String
}


class ChartViewModel: BaseViewModel, ObservableObject{
    
    let valueFormatter = ValueFormatter()
    
    @Published var historyData: [BarModel] = []
    @Published var period = Period.list[0]
}
