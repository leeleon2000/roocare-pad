//
//  HistoryView.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/7.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI
import Charts

struct HistoryView: View {
    
    @ObservedObject var viewModel: HistoryViewModel
    
    var body: some View {
        if(viewModel.showScan){
            DeviceSettingView(viewModel: DeviceSettingViewModel(cellViewModel: viewModel.cellViewModel).apply(closure: {
                $0.showBindAlert()
            }))
        }else{
            VStack(alignment: .leading){
                Picker(selection: Binding(get: {self.viewModel.periodIndex.value},
                                          set: {self.viewModel.periodIndex.accept($0)}),
                       label: Text("")) {
                    ForEach(0..<Period.list.count) { index in
                        Text(Period.list[index].name).tag(index)
                    }
                }.pickerStyle(SegmentedPickerStyle())
                
                ScrollView(.vertical){
                    VStack{
                        ChartView(viewModel: viewModel.chartViewModel)
                            .frame(height: 400)
                            .padding()
                        HistoryDataView(viewModel: self.viewModel)
                        HistoryWarningView(viewModel: NotificationListViewModel(deviceID: viewModel.cellViewModel.record.id))
                            .frame(height: 800)
                    }
                }
            }
            .onAppear{
                self.viewModel.start()
            }
            .onDisappear{
                self.viewModel.stop()
            }
            .navigationBarItems(trailing: NavigationLink(destination: DeviceSettingView(viewModel:
                                                                                            DeviceSettingViewModel(cellViewModel: viewModel.cellViewModel)
            )){
                Image("ic_setting")
            }).navigationBarTitle("History".localized())
        }
    }
}

//struct His toryView_Previews: PreviewProvider {
//    static var previews: some View {
//        HistoryView(viewModel: HistoryViewModel(cellViewModel: DeviceListCellViewModel.testViewModel))
//    }
//}
//
