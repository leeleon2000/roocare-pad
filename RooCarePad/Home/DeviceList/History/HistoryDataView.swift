//
//  HistoryDataView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/24.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct HistoryDataView: View {
    
    @ObservedObject var viewModel: HistoryViewModel
    
    var body: some View {
        VStack{
            
            Text("GENERAL").padding()
            
            HStack{
                Text("Average temp")
                Spacer()
                Text(unitManager.getDegreeText(value: viewModel.temperatureModel.avg))
                    .foregroundColor(UIColor.MainColor.color)
            }.defaultOutline()
          
            HStack{
                Text("Highest temp")
                Spacer()
                Text(unitManager.getDegreeText(value: viewModel.temperatureModel.high))
                    .foregroundColor(UIColor.MainColor.color)
            }.defaultOutline().padding(.top, 10)
            
            HStack{
                Text("Lowest temp")
                Spacer()
                Text(unitManager.getDegreeText(value: viewModel.temperatureModel.low))
                    .foregroundColor(UIColor.MainColor.color)
            }.defaultOutline().padding(.top, 10)
            
        }.padding()
    }
}

struct HistoryWarningView: View {
    
    @ObservedObject var viewModel: NotificationListViewModel

    var body: some View {
        VStack{
            Text("WARNINGS").padding()
            
            List{
                ForEach(0..<viewModel.list.count, id: \.self){ index in
                    let data = viewModel.list[index]
                    NotificationListCellView(viewModel: data)
                    .padding(4)
                    .defaultOutline()
                    .padding(10)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                    .listRowInsets(EdgeInsets())
                    .background(Color.white)
                }
            }
        }.padding()
        .onAppear{
            viewModel.start()
        }
    }
}
