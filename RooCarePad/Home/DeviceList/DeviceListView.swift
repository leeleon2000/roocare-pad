//
//  DeviceListView.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/5.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct DeviceListCellView: View {
    var autoSelection: String
    @Binding var selection: UUID?
  //  @Binding var showLink: Bool
    let viewModel: DeviceListCellViewModel
    var body: some View {
        NavigationLink(destination: HistoryView(viewModel: HistoryViewModel(cellViewModel: viewModel, selection: autoSelection)),
                       tag: UUID(uuidString: viewModel.record.uuid) ?? UUID(),
                       selection: $selection
        ){
            HStack{
                Image(systemName: "thermometer")
                Text(viewModel.record.name)
            }
        }.tag(self.viewModel.record.id)
    }
}

struct DeviceListView: View {
    
    @ObservedObject var viewModel: DeviceListViewModel
    @State var pushActive = false

    var body: some View {
        NavigationView{
            VStack{               
                List{
                    ForEach(0..<viewModel.deivceList.count, id: \.self){ index in
                        let data = viewModel.deivceList[index]
                        DeviceListCellView(autoSelection: viewModel.selection?.uuidString ?? "",
                                           selection: $viewModel.selection,
                                           viewModel: data)
                            .padding(4)
                            .defaultOutline()
                            .padding(6)
                            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                            .listRowInsets(EdgeInsets())
                            .background(UIColor(red: 0.999, green: 0.999, blue: 0.999, alpha: 1.0).color)
                           // .listRowBackground(UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.0).color)
                    }
                }
                
                Button(action: {
                    self.viewModel.showDeviceAlert()
                }){
                    Text("Add Device").frame(minWidth: 0,maxWidth: .infinity)
                }
                .overlay(RoundedRectangle(cornerRadius: 20).stroke(UIColor.MainColor.color, style: StrokeStyle(lineWidth: 3)))
                .padding()
                .background(UIColor.MainColor.color)
                .foregroundColor(Color.white)
            }.sheet(isPresented: $viewModel.isShowBindAlert){
                ScanDeviceView(showModal: self.$viewModel.isShowBindAlert, viewModel: ScanDeviceViewModel(record: viewModel.selectRecord!))
            }
            .navigationBarTitle("Devices")
        }.onAppear{
            //UITableView.appearance().separatorStyle = .none
            if(settingManager.isFirstTimeGuide){
                settingManager.isFirstTimeGuide = false
                self.pushActive = true
            }
            self.viewModel.start()
        }.textFieldAlert(isShowing: $viewModel.isShowNewDeviceAlert, save: viewModel.save, text: $viewModel.newDeviceName, title: "Add Device".localized())
        .sheet(isPresented: self.$pushActive, content: {
            GuideView()
        })
    }
}

struct DeviceListView_Previews: PreviewProvider {
    static var previews: some View {
        DeviceListView(viewModel: DeviceListViewModel())
    }
}

