//
//  GuideView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/25.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct GuideView: View {
    var body: some View {
        PageView([Image("Guide 1").resizable().aspectRatio(contentMode: .fit),
                  Image("Guide 2").resizable().aspectRatio(contentMode: .fit),
                  Image("Guide 3").resizable().aspectRatio(contentMode: .fit)])
    }
}

