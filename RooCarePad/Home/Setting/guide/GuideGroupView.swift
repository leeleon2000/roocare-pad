//
//  GuideGroupView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/11/4.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct GuideGroupView: View {
    var body: some View {
            VStack{
                NavigationLink(destination: GuideView()){
                    Text("Quick Start Guide").padding()
                        .defaultOutline()
                }.padding()
                NavigationLink(destination: WebContentView(url: "https://www.roocare.jp/privacy-policy")){
                    Text("How to change battery").padding()
                        .defaultOutline()
                }.padding()
                Spacer()
            }.navigationBarTitle("User Guides")
    }
}

struct GuideGroupView_Previews: PreviewProvider {
    static var previews: some View {
        GuideGroupView()
    }
}
