//
//  SettingNotificationView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/6.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import SwiftUI

struct SettingNotificationView: View {
    @ObservedObject var viewModel: SettingNotificationViewModel = SettingNotificationViewModel()
    var body: some View {
        VStack{
            Text("High Temp Alert")
            VStack{
                HStack{
                    Text("Higher than: \(String(format: "%.1f", self.viewModel.upperTemperatureAlertDegree)) °C")
                    Spacer()
                    Toggle("", isOn: $viewModel.isUpperTemperatureAlertOn)
                }.padding([.top, .leading, .trailing])
                Slider(value: $viewModel.upperTemperatureAlertDegree, in: 34...37.5, step: 0.1).padding()
            }.defaultOutline().padding()
            Spacer()
        }
    }
}
