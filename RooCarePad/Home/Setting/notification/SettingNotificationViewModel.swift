//
//  SettingNotificationViewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/6.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import SwiftUI

class SettingNotificationViewModel: BaseViewModel, ObservableObject {
    
    @Published var upperTemperatureAlertDegree: Double = settingManager.upperTemperatureAlert{
        willSet(value){
            settingManager.upperTemperatureAlert = value
        }
    }
    
    @Published var isUpperTemperatureAlertOn: Bool = settingManager.isUpperTemperatureAlertOn{
        willSet(value){
            settingManager.isUpperTemperatureAlertOn = value
        }
    }    
}
