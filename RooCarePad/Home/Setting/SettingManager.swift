//
//  SettingManager.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/7.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

let settingManager = SettingManager()

private let KEY_UpperTemperatureAlert = "KEY_UpperTemperatureAlert"
private let KEY_isUpperTemperatureAlertOn = "KEY_isUpperTemperatureAlertOn"
private let Key_degree_Unit = "Key_degree_Unit"
private let KEY_isFirstTimeLaunch = "KEY_isFirstTimeLaunch"
private let KEY_isFirstTimeGuide = "KEY_isFirstTimeGuide"
private let KEY_isFirstTimeAgreement = "KEY_isFirstTimeAgreement"
private let KEY_isLicensed = "KEY_isLicensed"


class SettingManager{
    
    var isBellOn: [UUID:Bool] = [:]
    
    var isLicensed: Bool{
        get{
            UserDefaults.standard.object(forKey: KEY_isLicensed) as? Bool ?? false
        }
        set(value){
            UserDefaults.standard.setValue(value, forKey: KEY_isLicensed)
        }
    }
    
    var isFirstTimeLaunch: Bool{
        get{
            UserDefaults.standard.object(forKey: KEY_isFirstTimeLaunch) as? Bool ?? true
        }
        set(value){
            UserDefaults.standard.setValue(value, forKey: KEY_isFirstTimeLaunch)
        }
    }
    
    var isFirstTimeGuide: Bool{
        get{
            UserDefaults.standard.object(forKey: KEY_isFirstTimeGuide) as? Bool ?? true
        }
        set(value){
            UserDefaults.standard.setValue(value, forKey: KEY_isFirstTimeGuide)
        }
    }
    
    var isFirstTimeAgreement: Bool{
        get{
            UserDefaults.standard.object(forKey: KEY_isFirstTimeAgreement) as? Bool ?? true
        }
        set(value){
            UserDefaults.standard.setValue(value, forKey: KEY_isFirstTimeAgreement)
        }
    }
    
    func shouldShowHighAlert(temperature: Float) -> Bool {
 //       return true
        return temperature > Float(upperTemperatureAlert) && isUpperTemperatureAlertOn
    }
    
    var upperTemperatureAlert: Double{
        get{
            UserDefaults.standard.object(forKey: KEY_UpperTemperatureAlert) as? Double ?? 37.5
        }
        set(value){
            UserDefaults.standard.setValue(value, forKey: KEY_UpperTemperatureAlert)
        }
    }
    
    var isUpperTemperatureAlertOn: Bool{
        get{
            UserDefaults.standard.object(forKey: KEY_isUpperTemperatureAlertOn) as? Bool ?? true
        }
        set(value){
            UserDefaults.standard.setValue(value, forKey: KEY_isUpperTemperatureAlertOn)
        }
    }
    
    var degreeUnit: String?{
        get{
            UserDefaults.standard.object(forKey: Key_degree_Unit) as? String
        }
        set(value){
            UserDefaults.standard.setValue(value, forKey: Key_degree_Unit)
        }
    }
}
