//
//  SettingView.swift
//  RooCarePad
//
//  Created by Leon on 2020/8/5.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct SettingListCellView: View {
    @ObservedObject var viewModel: SettingListCellViewModel
    var body: some View {
        NavigationLink(destination: viewModel.viewFactory(), isActive: $viewModel.isActive){
            HStack{
                Image(viewModel.imageName)
                    .resizable()
                    .scaledToFit()
                    .frame(minWidth: 2,
                           maxWidth: 20,
                           minHeight: 20,
                           maxHeight: 20,
                           alignment: .center)
                Text(viewModel.title.localized())
            }
        }
    }
}

struct SettingView: View {
    @ObservedObject var viewModel: SettingListViewModel
        
    var body: some View {
        NavigationView{
            // VStack{
//                List(viewModel.list){ data in
//                    SettingListCellView(viewModel: data)
//                }            
            List{
                ForEach(0..<viewModel.list.count){ data in
                    SettingListCellView(viewModel: viewModel.list[data])
                        .padding(4)
                        .defaultOutline()
                        .padding(10)
                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                        .listRowInsets(EdgeInsets())
                        .background(Color.white)
                }
            }.navigationBarTitle("Settings")
            //}
        }.onAppear{
           // UITableView.appearance().separatorStyle = .none
//            UITableView.appearance().separatorColor = .clear
        }
    }
}

struct SettingView_Previews: PreviewProvider {
    static var previews: some View {
        SettingView(viewModel: SettingListViewModel())
    }
}
