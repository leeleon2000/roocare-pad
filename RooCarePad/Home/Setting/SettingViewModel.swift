//
//  SettingViewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class SettingListCellViewModel: Identifiable, ObservableObject {
    
    internal init(id: UUID = UUID(),
                  title: String,
                  imageName: String,
                  isActive: Bool = false,
                  viewFactory: @escaping () -> AnyView = {
        AnyView(SettingDetailView())
    }) {
        self.id = id
        self.title = title
        self.imageName = imageName
        self.viewFactory = viewFactory
        self.isActive = isActive
    }
    
    var id = UUID()
    let title: String
    let imageName: String
    @Published var isActive = false
    var viewFactory: () -> AnyView = {
        AnyView(SettingDetailView())
    }
}

class SettingListViewModel: BaseViewModel, ObservableObject{
    @Published var list: [SettingListCellViewModel] = [
        SettingListCellViewModel(title: "Alerts", imageName: "ic_bell_on", viewFactory: {
            AnyView(SettingNotificationView())
        }),
     //   SettingListCellViewModel(title: "Corporate License", imageName: "ic_license"),
        SettingListCellViewModel(title: "Units", imageName: "ic_disclaimer"){
            AnyView(SettingUnitView())
        },
        SettingListCellViewModel(title: "Feedback", imageName: "ic_feedback"){
            AnyView(WebContentView(url: "https://www.roocare.jp/contact-us"))
        },
        SettingListCellViewModel(title: "User Guides", imageName: "ic_userguide"){
            AnyView(GuideGroupView())
        },
        SettingListCellViewModel(title: "Corporate License", imageName: "ic_license", isActive: homeTabManager.isShowLicense){
            AnyView(LicenseView(viewModel: LicenseViewModel()))
        },
        SettingListCellViewModel(title: "Terms of Use", imageName: "ic_doc"){
            AnyView(WebContentView(url: "https://www.roocare.jp/terms-of-use"))
        },
        SettingListCellViewModel(title: "Privacy Policy", imageName: "ic_lock"){
            AnyView(WebContentView(url: "https://www.roocare.jp/privacy-policy"))
        },
        SettingListCellViewModel(title: "Disclaimer", imageName: "ic_disclaimer"){
            AnyView(WebContentView(url: "https://www.roocare.jp/disclaimer"))
        }
     ]
}
