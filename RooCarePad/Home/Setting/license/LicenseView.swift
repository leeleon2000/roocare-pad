//
//  LicenseView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/11/5.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct LicenseView: View {
    
    @ObservedObject var viewModel: LicenseViewModel
    
    var body: some View {
        VStack{
            Text("Step1: Enter details").foregroundColor(UIColor.MainColor.color)
            TextField("Name", text: $viewModel.name)
                .defaultOutline()
                .padding([.leading, .trailing], 16)
                .padding(.top, 8)
            TextField("Company Name", text: $viewModel.name)
                .defaultOutline()
                .padding([.leading, .trailing], 16)
                .padding(.top, 8)
            TextField("Phone Number", text: $viewModel.name)
                .defaultOutline()
                .padding([.leading, .trailing], 16)
                .padding(.top, 8)
            TextField("Email", text: $viewModel.name)
                .defaultOutline()
                .padding([.leading, .trailing], 16)
                .padding(.top, 8)
            Button("Register", action: {}).defaultOutline().padding()
            
            Text("Step2: Please enter the code we have sent you").foregroundColor(UIColor.MainColor.color)
            TextField("Enter 6 digit code", text: $viewModel.code)
                .defaultOutline()
                .padding([.leading, .trailing], 16)
                .padding(.top, 8)
            
            Button("Activate", action: {viewModel.activate()})
                .frame(minWidth: 0,maxWidth: .infinity)
                .defaultSolidOutline()
                .padding()

            Spacer()
        }
        .onAppear{
            homeTabManager.isShowLicense = false
        }
        .alert(isPresented: $viewModel.showAlert){
            if(viewModel.isSuccess){
                return Alert(title: Text("Success"), message: Text(""), dismissButton: .default(Text("OK")))
            }else{
                return Alert(title: Text("Wrong Code"), message: Text(""), dismissButton: .default(Text("OK")))
            }
        }
        .navigationBarTitle("License")
    }
}

struct LicenseView_Previews: PreviewProvider {
    static var previews: some View {
        LicenseView(viewModel: LicenseViewModel())
    }
}
