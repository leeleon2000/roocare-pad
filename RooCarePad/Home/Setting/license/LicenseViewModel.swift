//
//  LicenseViewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/11/5.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

class LicenseViewModel: BaseViewModel, ObservableObject{
    @Published var name = ""
    @Published var code = ""
    @Published var showAlert = false
    @Published var isSuccess = false
    func activate(){
        if(code == "123456"){
            settingManager.isLicensed = true
            isSuccess = true
        }else{
            isSuccess = false
        }
        showAlert = true
    }
}
