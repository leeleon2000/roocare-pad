//
//  SettingDetailView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/9/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct SettingDetailView: View {
    var body: some View {
        Text("Hello, World!")
    }
    
    static func getFactory() -> ()-> SettingDetailView{
        return { SettingDetailView() } 
    }
}

struct SettingDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SettingDetailView()
    }
}
