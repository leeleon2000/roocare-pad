//
//  SettingUnitViewModel.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

class SettingUnitViewModel: BaseViewModel, ObservableObject{
    let degreeSettingNames = [DegreeUnit.C.rawValue, DegreeUnit.F.rawValue]
    
    let selected = unitManager.getDegreeUnit().rawValue
    func selectDegree(name: String){
        if(name == degreeSettingNames[0]){
            unitManager.saveDegreeUnit(unit: .C)
        }else{
            unitManager.saveDegreeUnit(unit: .F)
        }
    }
    
}
