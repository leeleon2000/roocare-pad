//
//  SettingUnitView.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import SwiftUI

struct SettingUnitView: View {
    @ObservedObject var viewModel = SettingUnitViewModel()
    var body: some View {
        VStack{
            Text("Temperature")
            RadioButtonGroups(names: viewModel.degreeSettingNames, callback: {
                viewModel.selectDegree(name: $0)
            }, selectedId: viewModel.selected).defaultOutline()
            Spacer()
        }.padding()
    }
}
