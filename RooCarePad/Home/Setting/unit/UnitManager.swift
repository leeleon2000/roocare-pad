//
//  UnitManager.swift
//  RooCarePad
//
//  Created by Leon Lee on 2020/10/15.
//  Copyright © 2020 Leon Lee. All rights reserved.
//

import Foundation

enum DegreeUnit: String{
    case C = "Celsius(°C)"
    case F = "Fahrenheit(°F)"
}

let unitManager = UnitManager()

class UnitManager{
    
    func saveDegreeUnit(unit: DegreeUnit){
        settingManager.degreeUnit = unit.rawValue
    }
    
    func getDegreeUnit() -> DegreeUnit{
        let unit = settingManager.degreeUnit
        return unit == DegreeUnit.F.rawValue ? .F : .C
    }
    
    func getDegreeText(value: Double) -> String{
        if(getDegreeUnit() == .C){
            return String(format: "%.1f°C", value)
        }
        return String(format: "%.1f°F", value)
    }
    
    func getDegreeValue(value: Float) -> Float{
        if(getDegreeUnit() == .C){
            return value
        }
        
        if(value == 0.0){
            return 0.0
        }
        
        return value * 1.8 + 32.0
    }
}
 
